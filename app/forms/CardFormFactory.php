<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


class CardFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;
    private $cardManager;

	public function __construct(FormFactory $factory, Model\CardManager $cardManager)
	{
		$this->factory = $factory;
        $this->cardManager = $cardManager;
	}

	public function create($match, $lengthOfMatch, $edit = false, $id = null){
	    $form = $this->factory->create();
        $minutes = array();
        for($i = 0; $i < $lengthOfMatch; $i++){
            $minute = $i < 10 ? "0".$i : "".$i;
            array_push($minutes, $minute);
        }
        $seconds = array();
        for($i = 0; $i < 60; $i++){
            $second = $i < 10 ? "0".$i : "".$i;
            array_push($seconds, $second);
        }
        $form->addSelect("minute", "Čas");
        $form["minute"]->setItems($minutes);
        $form->addSelect("second", ":");
        $form["second"]->setItems($seconds);
        $form->addSelect("player", "Hráč");
        if($edit === false) {
            $form->addRadioList("team", "Tým");
        }
        $form->addRadioList("typeOfCard", "Druh sankce", ["g" => "Zelená karta - varování",
            "y" => "Žlutá karta - výstraha", "r" => "Červená karta - vyloučení"]);
        $form->addHidden("match", $match);
        $form->onValidate[] = [$this, "validateNotPreviousRedCard"];
	    if($edit === true){
            $form->addHidden("card", $id);
            $form->addSubmit("editCard", "Editovat faul");
        } else {
            $form->addSubmit("addCard", "Přidat faul");
            $form->onSuccess[] = [$this, "addCard"];
        }
        return $form;
    }

    public function validateNotPreviousRedCard(Form $form, $values){
        $player = intval($values["player"]);
        $minute = intval($values["minute"]) < 10 ? "0".$values["minute"] : $values["minute"];
        $second = intval($values["second"]) < 10 ? "0".$values["second"] : $values["second"];
        $time = $minute.":".$second;
        $match = intval($values["match"]);
        $redCard = $this->cardManager->hasPlayerGotARedCard($match, $player, $time);
        if($redCard == true){
            $form->addError("Nelze udělit další trest hráči, který již dostal červenou kartu a byl vyloučen.");
        }
    }

    public function addCard(Form $form, $values){
        $minute = intval($values["minute"]) < 10 ? "0".$values["minute"] : $values["minute"];
        $second = intval($values["second"]) < 10 ? "0".$values["second"] : $values["second"];
        $time = $minute.":".$second;
        $player = $values["player"];
        $typeOfCard = $values["typeOfCard"];
        $match = $values["match"];
        $this->cardManager->addCard($typeOfCard, $time, $player, $match);
    }


}
