<?php

namespace App\Forms;

use App\Model\CategoryManager;
use Nette;
use Nette\Application\UI\Form;


class CategoryFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var CategoryManager */
	private $categoryManager;

    /**
     * AddCategoryFormFactory constructor.
     * @param FormFactory $factory
     * @param CategoryManager $categoryManager
     */
	public function __construct(FormFactory $factory, CategoryManager $categoryManager)
	{
		$this->factory = $factory;
		$this->categoryManager = $categoryManager;
	}

    /**
     * @return CategoryManager
     */
    public function getCategoryManager()
    {
        return $this->categoryManager;
    }

    /**
     * @return Form
     */
    public function createFilterForm(){
        $form = $this->factory->create();
        $form->addText("title", "Označení");
        $form->addInteger("ageTo", "Věk do");
        $form->addCheckboxList('sex', "Pohlaví", ["male" => "Muži", "female" => "Ženy", "unknown" => "Neznámé pohlaví"]);
        $form->addSubmit("filterCategories", "Vyhledat");
        return $form;
    }
    /**
     * @param $edit
     * @param null $id
     * @return Form
     */
	public function create($edit, $id = null)
	{
		$form = $this->factory->create();
        $form->addText('title', 'Označení')->setRequired(true)->
        addRule(Form::MAX_LENGTH, "Označení kategorie nemůže být delší než 20 znaků", 20);
        $form->addCheckboxList('sex', "Pohlaví", ["male" => "Muži", "female" => "Ženy", "unknown" => "Neznámé pohlaví"]);
        $form->addInteger('ageTo', "Věk do")->setRequired(false)->
        addRule(Form::MIN, "Horní věková hranice hráčů musí být nejméně 3 roky.", 3)
            ->addRule(Form::MAX,"Horní věková hranice hráčů nesmí být vyšší než 125 let.",125);

        if($edit == true){
            $form->addHidden("id", $id);
            $form->addSubmit("editCategory", "Upravit kategorii");
        } else {
            $form->addSubmit("addCategory","Přidat kategorii");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditCategory($form, $values, false);
            };
        }
        $form->onValidate[] = function(Form $form, $values){
            if(count($values["sex"]) === 0){
                $form->addError("Musí být zvoleno alespoň jedno pohlaví hráčů.");
            }
        };
		return $form;
	}


    /**
     * @param Form $form
     * @param $values
     * @param $edit
     * @param null $id
     */
	public function addOrEditCategory(Form $form, $values, $edit, $id = null){
        $title = $values["title"];
        $sexArray = $values["sex"];
        $sex = 0;
        if(array_search("male", $sexArray) !== false){
            $sex+=4;
        }
        if(array_search("female", $sexArray) !== false){
            $sex+=2;
        }
        if(array_search("unknown", $sexArray) !== false){
            $sex+=1;
        }
        $ageTo = $values["ageTo"];
        if($edit === true){
            $this->categoryManager->editCategory($id, $title, $sex, $ageTo);
        } else {
            $this->categoryManager->addCategory($title, $sex, $ageTo);
        }
    }
}
