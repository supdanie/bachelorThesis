<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


class GameEventFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;
    private $playerManager;
    private $tourneyManagementManager;
    private $matchManager;
    public function __construct(FormFactory $factory, Model\PlayerManager $playerManager,
    Model\TourneyManagementManager $tourneyManagementManager, Model\MatchManager $matchManager)
    {
        $this->factory = $factory;
        $this->playerManager = $playerManager;
        $this->tourneyManagementManager = $tourneyManagementManager;
        $this->matchManager = $matchManager;
    }

    public function getPlayersInTeam($tourneyID, $teamID){
        $playersInTeam = $this->tourneyManagementManager->getPlayersByTeam($tourneyID, $teamID);
        $arrayOfPlayers = array();
        foreach($playersInTeam as $player){
            $playerID = $player[Model\TourneyManagementManager::SECOND_COLUMN_PLAYER];
            $playerEntity = $this->playerManager->get($playerID);
            $name = $playerEntity[Model\PlayerManager::COLUMN_NAME]." ".
                $playerEntity[Model\PlayerManager::COLUMN_SURNAME];
            $arrayOfPlayers[$playerID] = $name;
        }
        return $arrayOfPlayers;
    }

    public function getPlayersForForm($tourneyID, $matchID){
        $match = $this->matchManager->get($matchID);
        $firstTeam = $match[Model\MatchManager::COLUMN_FIRST_TEAM];
        $secondTeam = $match[Model\MatchManager::COLUMN_SECOND_TEAM];
        $arrayWithEmptyPlayer = [-1 => "Všichni hráči"];
        $playersInFirstTeam = $this->getPlayersInTeam($tourneyID, $firstTeam);
        $playersInSecondTeam = $this->getPlayersInTeam($tourneyID, $secondTeam);
        return $arrayWithEmptyPlayer + $playersInFirstTeam + $playersInSecondTeam;
    }

    public function createFilterForm($tourney, $match, $lengthOfMatch){
        $form = $this->factory->create();
        $minutes = array();
        $lastMinute = "00";
        for($i = 0; $i < $lengthOfMatch; $i++){
            $minute = $i < 10 ? "0".$i : "".$i;
            $minutes[$minute] = $minute;
            $lastMinute = $minute;
        }
        $seconds = array();
        for($i = 0; $i < 60; $i++){
            $second = $i < 10 ? "0".$i : "".$i;
            $seconds[$second] = $second;
        }
        $form->addCheckboxList("typeOfGameEvent", "Typ herní události:", ["g" => "Gól", "c" => "Faul"]);
        $form->addSelect("player", "Hráč");
        $players = $this->getPlayersForForm($tourney, $match);
        $form["player"]->setItems($players);
        $form->addSelect("minuteFrom","Čas od");
        $form["minuteFrom"]->setItems($minutes);
        $form->addSelect("secondFrom", "");
        $form["secondFrom"]->setItems($seconds);
        $form->addSelect("minuteTo","Čas do");
        $form["minuteTo"]->setItems($minutes);
        $form->addSelect("secondTo", "");
        $form["secondTo"]->setItems($seconds);
        $form->addHidden("match", $match);
        $form->addSubmit("filterGameEvents", "Vyhledat herní události");
        $form->setDefaults(["minuteTo" => $lastMinute,
                            "secondTo" => "59"]);
        return $form;
    }
}
