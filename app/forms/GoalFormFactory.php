<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


class GoalFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;
    private $goalManager;
    private $cardManager;
    public function __construct(FormFactory $factory, Model\GoalManager $goalManager,
    Model\CardManager $cardManager)
    {
        $this->factory = $factory;
        $this->goalManager = $goalManager;
        $this->cardManager = $cardManager;
    }


    public function create($match, $lengthOfMatch, $edit = false, $id = null){
        $form = $this->factory->create();
        $minutes = array();
        for($i = 0; $i < $lengthOfMatch; $i++){
            $minute = $i < 10 ? "0".$i : "".$i;
            array_push($minutes, $minute);
        }
        $seconds = array();
        for($i = 0; $i < 60; $i++){
            $second = $i < 10 ? "0".$i : "".$i;
            array_push($seconds, $second);
        }
        $form->addSelect("minute", "Čas");
        $form["minute"]->setItems($minutes);
        $form->addSelect("second", ":");
        $form["second"]->setItems($seconds);
        $form->addSelect("player", "Hráč");
        $form->addHidden("match", $match);
        $form->onValidate[] = [$this, "validateNotPreviousRedCard"];
        if($edit === true){
            $form->addHidden("goal", $id);
            $form->addSubmit("editGoal", "Editovat gól");
        } else {
            $form->addSubmit("addGoal", "Přidat gól");
            $form->onSuccess[] = [$this, "addGoal"];
        }
        return $form;
    }


    public function validateNotPreviousRedCard(Form $form, $values){
        $player = intval($values["player"]);
        $minute = intval($values["minute"]) < 10 ? "0".$values["minute"] : $values["minute"];
        $second = intval($values["second"]) < 10 ? "0".$values["second"] : $values["second"];
        $time = $minute.":".$second;
        $match = intval($values["match"]);
        $redCard = $this->cardManager->hasPlayerGotARedCard($match, $player, $time);
        if($redCard == true){
            $form->addError("Hráč, který již dostal červenou kartu a byl vyloučen, nemůže v rámci zápasu dát další gól.");
        }
    }

    public function addGoal(Form $form, $values){
        $minute = intval($values["minute"]) < 10 ? "0".$values["minute"] : $values["minute"];
        $second = intval($values["second"]) < 10 ? "0".$values["second"] : $values["second"];
        $time = $minute.":".$second;
        $player = $values["player"];
        $match = $values["match"];
        $this->goalManager->addGoal($time, $player, $match);
    }
}
