<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


class PlayerFormFactory
{
	use Nette\SmartObject;
	/** @var FormFactory */
	private $factory;

	/** @var Model\PlayerManager */
	private $playerManager;


	public function __construct(FormFactory $factory, Model\PlayerManager $playerManager)
	{
		$this->factory = $factory;
		$this->playerManager = $playerManager;
	}

    /**
     * @return Model\PlayerManager
     */
    public function getPlayerManager()
    {
        return $this->playerManager;
    }

    public function createFilterForm(){
        $form = $this->factory->create();
        $form->addText("name", "Jméno");
        $form->addText("surname", "Příjmení");
        $form->addDatePicker("birthdateFrom", "Datum narození od");
        $form->addDatePicker("birthdateTo", "do");
        $form->addCheckboxList("sex", "Pohlaví", ["male" => "Muž", "female" => "Žena", "unknown" => "Neznámé"]);
        $form->addInteger("number", "Číslo");
        $form->addSubmit("filterPlayers", "Vyhledat");
        $form->onValidate[] =  function(Form $form, $values){
            if(!empty($values["birthdateFrom"]) && !empty($values["birthdateTo"]) &&
                $values["birthdateFrom"] > $values["birthdateTo"]){
                $form->addError("Nejdřívější datum narození nesmí být pozdější než nejpozdější datum narození.");
            }
        };
        return $form;
    }
    /**
     * @param $edit
     * @param null $id
     * @return Form
     */
	public function create($edit, $id = null)
	{
		$form = $this->factory->create();
        $form->addText("name", "Jméno")->setRequired(true)->
        addRule(Form::MIN_LENGTH,"Jméno nemůže být kratší než 2 znaky.",2)
            ->addRule(Form::MAX_LENGTH, "Jméno nemůže být delší než 50 znaků.", 50);
        $form->addText("surname", "Příjmení")->setRequired(true)->
        addRule(Form::MIN_LENGTH,"Příjmení nemůže být kratší než 2 znaky.", 2)
            ->addRule(Form::MAX_LENGTH,"Příjmení nemůže být delší než 50 znaků.", 50);
        $form->addRadioList("sex", "Pohlaví", ["male" => "Muž", "female" => "Žena", "unknown" => "Neznámé"])
            ->setRequired(true);
        $form->addDatePicker("birthdate", "Datum narození")->setRequired(true);
        $form->addInteger("number", "Číslo");
        if($edit === true){
            $form->addHidden("id", $id);
            $form->addSubmit("editPlayer","Upravit hráče");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditPlayer($form, $values, true, $values["id"]);
            };
        } else {
            $form->addSubmit("addPlayer", "Přidat hráče");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditPlayer($form, $values, false, null);
            };
        }
        $form->onValidate[] =  function(Form $form, $values){
            if($values["birthdate"] > new Nette\Utils\DateTime()){
                $form->addError("Hráč nemůže být narozen později než dnes.");
            }
        };
		return $form;
	}

    /**
     * @param Form $form
     * @param $values
     * @param $edit
     * @param $id
     */
	public function addOrEditPlayer(Form $form, $values, $edit, $id){
	    $name = $values["name"];
        $surname = $values["surname"];
        $sex = $values["sex"][0];
        $birthdate = new \DateTime($values["birthdate"]);
        $number = $values["number"];
        if($edit == true){
            $this->playerManager->editPlayer($id, $name, $surname, $birthdate, $sex, $number);
        } else {
            $this->playerManager->addPlayer($name, $surname, $birthdate, $sex, $number);
        }
    }
}
