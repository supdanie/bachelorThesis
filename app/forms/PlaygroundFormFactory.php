<?php

namespace App\Forms;

use App\Model;
use Bazo\Forms\Controls\ColorPicker;
use Bazo\Forms\Controls\DatePicker;
use Nette;
use Nette\Application\UI\Form;


class PlaygroundFormFactory
{
	use Nette\SmartObject;


	/** @var FormFactory */
	private $factory;

	/** @var Model\PlaygroundManager */
	private $playgroundManager;


	public function __construct(FormFactory $factory, Model\PlaygroundManager $playgroundManager)
	{
		$this->factory = $factory;
		$this->playgroundManager = $playgroundManager;
	}

	/**
	 * @return Form
	 */
	public function create($edit, $id = null)
	{
		$form = $this->factory->create();
        $form->addText('color', 'Barva')->getControlPrototype()->type('color');
        $form->addDatePicker('dateOfFoundation', 'Datum založení');
        $form->addGMap("position", "GPS")->setWidth(400)->setHeight(120);
        $form->addTextArea("description", "Popis");
        if($edit == true){
            $form->addHidden("id", $id);
            $form->addSubmit("editPlayground", "Editovat hřiště");
        } else {
            $form->addSubmit("addPlayground", "Přidat hřiště");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditPlayground($values, false);
            };
        }
		return $form;
	}

    public function createFilterForm(){
        $form = $this->factory->create();
        $form->addDatePicker("from", "Datum založení od");
        $form->addDatePicker("to", "Datum založení do");
        $form->addText('color', 'Barva')->getControlPrototype()->type('color');
        $form->addSubmit("filterPlaygrounds", "Vyhledat hřiště");
        $form->onValidate[] =  function(Form $form, $values){
            if(!empty($values["from"]) && !empty($values["to"]) &&
                $values["from"] > $values["to"]){
                $form->addError("Nejdřívější datum založení hřiště nesmí být pozdější než nejpozdější datum založení hřiště.");
            }
        };
        return $form;
    }
    /**
     * @param $values
     * @param $edit
     * @param null $id
     */
	public function addOrEditPlayground($values, $edit, $id = null){
        $colour = $values["color"];
        $dateOfFoundation = new \DateTime($values["dateOfFoundation"]);
        $GPS = $values["position"]["latitude"].",".$values["position"]["longitude"];
        $description = $values["description"];
        if($edit == false){
            $this->playgroundManager->addPlayground($colour, $GPS, $dateOfFoundation, $description);
        } else {
            $this->playgroundManager->editPlayground($id, $colour, $GPS, $dateOfFoundation, $description);
        }
    }
}