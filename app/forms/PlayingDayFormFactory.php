<?php

namespace App\Forms;

use App\Model;
use Bazo\Forms\Controls\ColorPicker;
use Bazo\Forms\Controls\DatePicker;
use Nette;
use Nette\Application\UI\Form;


class PlayingDayFormFactory
{
	use Nette\SmartObject;


	/** @var FormFactory */
	private $factory;

	/** @var Model\PlayingDayManager */
	private $playingDayManager;


	public function __construct(FormFactory $factory, Model\PlayingDayManager $playingDayManager)
	{
		$this->factory = $factory;
		$this->playingDayManager = $playingDayManager;
	}


	public function createFilterForm(){
	    $form = $this->factory->create();
        $form->addDatePicker("from", "Od");
        $form->addDatePicker("to", "Do");
        $form->addSubmit("filterPlayingDays", "Vyhledat hrací dny");
        $form->onValidate[] =  function(Form $form, $values){
            if(!empty($values["from"]) && !empty($values["to"]) &&
                $values["from"] > $values["to"]){
                $form->addError("Nejdřívější datum nesmí být pozdější než nejpozdější datum.");
            }
        };
        return $form;
    }
	/**
	 * @return Form
	 */
	public function create($edit, $playground, $id = null)
	{
		$form = $this->factory->create();
        $form->addDatePicker('day', 'Datum');
        $hours = array();
        for($i = 0; $i < 24; $i++){
            $hour = $i < 10 ? "0".$i : "".$i;
            array_push($hours, $hour);
        }
        $minutes = array();
        for($i = 0; $i < 60; $i++){
            $minute = $i < 10 ? "0".$i : "".$i;
            array_push($minutes, $minute);
        }
        $form->addSelect("fromHour", "Od")->setRequired(true);
        $form["fromHour"]->setItems($hours);
        $form->addSelect("fromMinute", ":")->setRequired(true);
        $form["fromMinute"]->setItems($minutes);
        $form->addSelect("toHour", "Do")->setRequired(true);
        $form["toHour"]->setItems($hours);
        $form->addSelect("toMinute", ":")->setRequired(true);
        $form["toMinute"]->setItems($minutes);
        $form->addHidden("playground", $playground);
        if($edit === true){
            $form->addHidden("id", $id);
            $form->addSubmit("editPlayingDay", "Editovat hrací den");
        } else {
            $form->addSubmit("addPlayingDay", "Přidat hrací den");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditPlayingDay($values, false, $values["playground"]);
            };
        }
        $form->onValidate[] = [$this, "checkPlayingDay"];
		return $form;
	}

	public function checkPlayingDay(Form $form, $values){
	    $hourFrom = intval($values["fromHour"]);
        $minuteFrom = intval($values["fromMinute"]);
        $hourTo = intval($values["toHour"]);
        $minuteTo = intval($values["toMinute"]);
        if($hourFrom > $hourTo){
            $form->addError("Čas začátku hracího dne musí být dřívější než konec hracího dne.");
        } elseif($hourFrom == $hourTo && $minuteFrom >= $minuteTo){
            $form->addError("Čas začátku hracího dne musí být dřívější než konec hracího dne.");
        }
        $day = $values["day"];
        $playground = intval($values["playground"]);
        $from = $this->getTwoNumbersFromOneNumber($hourFrom).":".$this->getTwoNumbersFromOneNumber($minuteFrom);
        $to = $this->getTwoNumbersFromOneNumber($hourTo).":".$this->getTwoNumbersFromOneNumber($minuteTo);
        $playingDay = !isset($values["id"]) ? null : intval($values["id"]);
        if($this->playingDayManager->existsPlayingDayAtThisTime($playground, $day, $from, $to, $playingDay)){
            $form->addError("Nemůže být vložen hrací den do jiného hracího dne.");
        }
    }

	public function getTwoNumbersFromOneNumber($numberString){
	    return strlen($numberString) == 1 ? "0".$numberString : $numberString;
    }
	public function addOrEditPlayingDay($values, $edit, $playground, $id = null){
        $day = $values["day"];
        $from = $this->getTwoNumbersFromOneNumber($values["fromHour"]).":".$this->getTwoNumbersFromOneNumber($values["fromMinute"]);
        $to = $this->getTwoNumbersFromOneNumber($values["toHour"]).":".$this->getTwoNumbersFromOneNumber($values["toMinute"]);
        if($edit === false){
            $this->playingDayManager->addPlayingDay($day, $from, $to, $playground);
        } else {
            $this->playingDayManager->editPlayingDay($id,$day, $from, $to, $playground);
        }
    }
}