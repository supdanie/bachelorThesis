<?php

namespace App\Forms;

use App\Model\TeamManager;
use App\Model\CategoryManager;
use Nette;
use Nette\Application\UI\Form;


class TeamFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var TeamManager */
	private $teamManager;
    private $categoryManager;
    /**
     * TeamFormFactory constructor.
     * @param FormFactory $factory
     * @param TeamManager $teamManager
     */
	public function __construct(FormFactory $factory, TeamManager $teamManager,
                                CategoryManager $categoryManager)
	{
		$this->factory = $factory;
		$this->teamManager = $teamManager;
        $this->categoryManager = $categoryManager;
	}


    /**
     * @return TeamManager
     */
    public function getTeamManager()
    {
        return $this->teamManager;
    }

    public function getArrayOfCategories(){
        $array = array();
        $categories = $this->categoryManager->getAll();
        foreach($categories as $category){
            $categoryID = $category[CategoryManager::COLUMN_ID];
            $array[$categoryID] = $category[CategoryManager::COLUMN_TITLE];
        }
        return $array;
    }

    public function createFilterForm(){
        $form = $this->factory->create();
        $form->addText("title", "Název:");
        $form->addText("abbreviation", "Zkratka");
        $form->addSelect("category", "Kategorie")->setAttribute("placeholder","        ");
        $form->addText("primaryColour", "Primární barva")->getControlPrototype()->type('color');
        $form->addText("secondaryColour", "Sekundární barva")->getControlPrototype()->type('color');
        $form->addSubmit("filterTeams", "Vyhledat");

        $categoriesForFilterForm = [-1 => "Všechny kategorie"];
        $categoriesForFilterForm = $categoriesForFilterForm+$this->getArrayOfCategories();
        $form["category"]->setItems($categoriesForFilterForm);
        return $form;
    }

    /**
     * @param $edit
     * @param null $id
     * @return Form
     */
	public function create($edit, $id = null)
	{
		$form = $this->factory->create();
        $form->addText("title","Název")->setRequired(true)->
        addRule(Form::MIN_LENGTH, "Název týmu musí obsahovat alespoň 2 znaky.", 2)->
        addRule(Form::MAX_LENGTH, "Název týmu nemůže být delší než 50 znaků", 50);
        $form->addText("abbreviation","Zkratka")
            ->setRequired(true)->addRule(Form::MAX_LENGTH, "Zkratka týmu nemůže být delší než 5 znaků", 5);
        $form->addSelect("category", "Kategorie")->setAttribute('placeholder', '        ')
            ->setRequired(true);
        $form->addText("primaryColour", "Primární barva")->getControlPrototype()->type('color');
        $form->addText("secondaryColour", "Sekundární barva")->getControlPrototype()->type('color');
        if($edit == true){
            $form->addHidden("id", $id);
            $form->addSubmit("editTeam", "Upravit tým");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditTeam($form, $values, true, $values["id"]);
            };
        } else {
            $form->addSubmit("addTeam", "Přidat tým");
            $form->onSuccess[] = function (Form $form, $values){
                $this->addOrEditTeam($form, $values, false, null);
            };
        }
        $form["category"]->setItems($this->getArrayOfCategories());
		return $form;
	}

    /**
     * @param Form $form
     * @param $values
     * @param $edit
     * @param $id
     */
	public function addOrEditTeam(Form $form, $values, $edit, $id){
	    $title = $values["title"];
        $abbreviation = $values["abbreviation"];
        $category = $values["category"];
        $primaryColour = $values["primaryColour"];
        $secondaryColour = $values["secondaryColour"];
        if($edit === true){
            $this->teamManager->editTeam($id, $title, $abbreviation, $category, $primaryColour, $secondaryColour);
        } else {
            $this->teamManager->addTeam($title, $abbreviation, $category, $primaryColour, $secondaryColour);
        }
    }
}
