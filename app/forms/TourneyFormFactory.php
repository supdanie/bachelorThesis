<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


class TourneyFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var Model\TourneyManager */
	private $tourneyManager;
    private $tourneyManagementManager;
    private $playgroundManager;
    private $tourneyExporter;
    private $tourneyImporter;
    private $categoryManager;
    private $refereeManager;
    private $playerManager;
    private $teamManager;
    private $matchManager;
	public function __construct(FormFactory $factory, Model\TourneyManager $tourneyManager,
                                Model\TourneyManagementManager $tourneyManagementManager,
                                Model\PlaygroundManager $playgroundManager,
        Model\ExportAndImport\TourneyExporter $tourneyExporter,
        Model\ExportAndImport\TourneyImporter $tourneyImporter,
        Model\CategoryManager $categoryManager,
        Model\RefereeManager $refereeManager, Model\PlayerManager $playerManager,
        Model\TeamManager $teamManager, Model\MatchManager $matchManager)
	{
		$this->factory = $factory;
		$this->tourneyManager = $tourneyManager;
        $this->tourneyManagementManager = $tourneyManagementManager;
        $this->playgroundManager = $playgroundManager;
        $this->tourneyExporter = $tourneyExporter;
        $this->tourneyImporter = $tourneyImporter;
        $this->categoryManager = $categoryManager;
        $this->refereeManager = $refereeManager;
        $this->playerManager = $playerManager;
        $this->teamManager = $teamManager;
        $this->matchManager = $matchManager;
	}


	public function createSetCustomRulesForm($tourney){
	    $categories = $this->categoryManager->getAll();
        $secondCategories = $this->categoryManager->getAll();
        $form = $this->factory->create();
        foreach ($categories as $firstCategory){
            foreach($secondCategories as $secondCategory){
                $firstCategoryID = $firstCategory[Model\CategoryManager::COLUMN_ID];
                $secondCategoryID = $secondCategory[Model\CategoryManager::COLUMN_ID];
                $name = $firstCategoryID."x".$secondCategoryID;
                $form->addCheckbox($name,"")
                    ->setDefaultValue($this->refereeManager->canBeARefereeOf($tourney, $firstCategoryID, $secondCategoryID));
            }
        }
        $form->addHidden("tourney", $tourney);
        $form->addSubmit("setCustomRules", "Nastavit pravidla");
        $form->onValidate[] = [$this, "validateCustomRules"];
        $form->onSuccess[] = [$this, "setCustomRulesForTourney"];
        return $form;
    }

    public function validateCustomRules(Form $form, $values){
        $categories = $this->categoryManager->getAll();
        $tourneyID = intval($values["tourney"]);
        $secondCategories = $this->categoryManager->getAll();
        foreach ($categories as $firstCategory){
            $existsAnyReferingCategory = false;
            $firstCategoryID = $firstCategory[Model\CategoryManager::COLUMN_ID];
            foreach($secondCategories as $secondCategory){
                $secondCategoryID = $secondCategory[Model\CategoryManager::COLUMN_ID];
                if($values[$secondCategoryID."x".$firstCategoryID] == true){
                    $existsAnyReferingCategory = true;
                    break;
                }
            }
            if($existsAnyReferingCategory == false){
                $firstCategoryTitle = $firstCategory[Model\CategoryManager::COLUMN_TITLE];
                if($this->tourneyManagementManager->existsAnyTeamInCategoryAtTourney($tourneyID, $firstCategoryID)){
                    $form->addError("Kategorii ".$firstCategoryTitle.", ve které jsou přihlášeny týmy na turnaj, nemůže rozhodovat žádná kategorie. Každou kategorii, ve které je nějaký tým přihlášen na turnaj, by měla mít možnost pískat alespoň jedna kategorie");
                }
            }
        }
    }


    public function addRefereeRulesForCategories($values, $tourney, $firstCategory, $secondCategory){
        if($values[$firstCategory."x".$secondCategory] == true){
            $teamsInCategory = $this->teamManager->getTeamsInCategory($firstCategory);
            foreach($teamsInCategory as $team){
                $teamID = $team[Model\TeamManager::COLUMN_ID];
                $this->refereeManager->addRefereeRule($tourney, $teamID, $secondCategory);
            }
        }
    }

    public function setCustomRulesForTourney(Form $form, $values){
        $tourneyID = intval($values["tourney"]);
        $categories = $this->categoryManager->getAll();
        $secondCategories = $this->categoryManager->getAll();
        foreach ($categories as $firstCategory){
            $firstCategoryID = $firstCategory[Model\CategoryManager::COLUMN_ID];
            foreach($secondCategories as $secondCategory){
                $secondCategoryID = $secondCategory[Model\CategoryManager::COLUMN_ID];
                $this->addRefereeRulesForCategories($values, $tourneyID, $firstCategoryID, $secondCategoryID);
            }
        }
    }

    public function getCategoriesForForm($tourney){
        $teams = $this->tourneyManagementManager->getTeamsByTourneySortedBySeedAndID($tourney);
        $categories = array();
        foreach($teams as $teamEntity){
            $teamID = $teamEntity[Model\TourneyManagementManager::COLUMN_TEAM];
            $team = $this->teamManager->get($teamID);
            if(!in_array($team[Model\TeamManager::COLUMN_CATEGORY], $categories)){
                array_push($categories, $team[Model\TeamManager::COLUMN_CATEGORY]);
            };
        }
        return $categories;
    }

    public function getNumberOfTeamsInCategory($tourney, $category){
        $i = 0;
        $teams = $this->tourneyManagementManager->getTeamsByTourney($tourney);
        foreach($teams as $teamEntity){
            $teamID = $teamEntity[Model\TourneyManagementManager::COLUMN_TEAM];
            $team = $this->teamManager->get($teamID);
            if($team[Model\TeamManager::COLUMN_CATEGORY] == $category){
                $i++;
            };
        }
        return $i;
    }

    public function createGenerateMatchScheduleForm($tourney){
        $form = $this->factory->create();
        $categories = $this->getCategoriesForForm($tourney);
        foreach($categories as $category) {
            $categoryEntity = $this->categoryManager->get($category);
            $title = $categoryEntity[Model\CategoryManager::COLUMN_TITLE];
            $maxGroups = intval($this->getNumberOfTeamsInCategory($tourney, $category) / 2);
            $form->addInteger($category, "Počet skupin v kategorii " . $title)->setRequired(true)
                ->addRule(Form::MIN, "Počet skupin v kategorii " . $title . " musí být kladné číslo.", 1)
                ->addRule(Form::MAX, "Počet skupin nesmí převýšit polovinu počtu týmů v kategorii " . $title . ". 
                            Kategorie může obsahovat maximálně " . $maxGroups . " skupin.", $maxGroups);
        }
        $form->addRadioList("systemOfDivisionToGroups", "Systém rozdělení týmů do skupin",["left" => "Zleva doprava",
            "leftRightLeft" => "Střídavě zleva doprava a zprava doleva"]);
        $form->addHidden("tourney", $tourney);
        $form->addSubmit("generateMatchSchedule", "Vygenerovat rozpis zápasů");
        $form->onSuccess[] = [$this,"generateMatchSchedule"];
        return $form;
    }

    public function generateMatchSchedule(Form $form, $values){
        $categories = $this->getCategoriesForForm(intval($values["tourney"]));
        $categoriesWithGroups = array();
        foreach($categories as $category){
            $categoriesWithGroups[$category] = intval($values[$category]);
        }
        $exportedTourney = $this->tourneyExporter->exportTourney(intval($values["tourney"]), $categoriesWithGroups);
        $input = $exportedTourney->toJson();
        $dir = "../generator/";
        file_put_contents("out", $input);
        exec($dir."a.out < out", $output, $return);
        $json = "";
        foreach($output as $partOfOutput){
            $json.=$partOfOutput;
        }
        var_dump($json);
        /* $schedule = Nette\Utils\Json::decode($json, Nette\Utils\Json::FORCE_ARRAY);
        $this->tourneyImporter->addMatches($schedule); */
    }

    public function createChangeSeedForm($tourney, $team){
        $form = $this->factory->create();
        $form->addInteger("seed", "Seed");
        $form->addHidden("tourney", $tourney);
        $form->addHidden("team", $team);
        $form->onSuccess[] = function(Form $form, $values){
            $this->changeSeed($values);
        };
        $form->addSubmit("updateSeed", "Upravit seed");
        return $form;
    }


    public function getSuitablePlayers($tourney, $idOfThePlayer){
        $teamsWithPlayer = $this->tourneyManagementManager->getTeamByPlayerAndTourney($tourney, $idOfThePlayer);
        $teamID = null;
        foreach($teamsWithPlayer as $team){
            $teamID = $team[Model\TourneyManagementManager::SECOND_COLUMN_TEAM];
        }
        $team = $this->teamManager->get($teamID);
        $categoryID = $team[Model\TeamManager::COLUMN_CATEGORY];
        $category = $this->categoryManager->get($categoryID);
        $ageTo = $category[Model\CategoryManager::COLUMN_AGE_TO];
        $sex = $category[Model\CategoryManager::COLUMN_SEX];
        $allPlayers = array();
        $tourneyEntity = $this->tourneyManager->get($tourney);
        $tourneyFrom = $tourneyEntity[Model\TourneyManager::COLUMN_FROM];
        $players = $this->playerManager->getSuitablePlayersForCategory($tourneyFrom, $sex, $ageTo);
        foreach($players as $player){
            $allPlayers[$player[Model\PlayerManager::COLUMN_ID]] = $player[Model\PlayerManager::COLUMN_NAME]." ".$player[Model\PlayerManager::COLUMN_SURNAME];
        }
        $suitablePlayers = array();
        foreach($allPlayers as $playerID => $playerName){
            $teamsWithPlayer = $this->tourneyManagementManager->getTeamByPlayerAndTourney
            ($tourney,$playerID);
            if($playerID === $idOfThePlayer){
                $suitablePlayers[$playerID] = $playerName;
            } else if($teamsWithPlayer->count() === 0){
                $suitablePlayers[$playerID] = $playerName;
            }
        }
        return $suitablePlayers;
    }
    public function createChangePlayerForm($tourney, $player){
        $form = $this->factory->create();
        $form->addText("player")->setRequired(true);
        $form->addHidden("tourney",$tourney);
        $form->addHidden("actualPlayer", $player);
        $form->addSubmit("removePlayer", "Odebrat");
        $form->addSubmit("changePlayer", "Výměna hráče");
        $form->onSuccess[] = function(Form $form, $values){
            $startOfID = strrpos($values["player"],"(")+1;
            $endOfID = strrpos($values["player"],")")-1;
            $values["player"] = intval(substr($values["player"], $startOfID, $endOfID));
            if($form["changePlayer"]->isSubmittedBy()){
                $this->changePlayer($values);
            } elseif($form["removePlayer"]->isSubmittedBy()){
                $this->tourneyManagementManager->removePlayerFromTourney(intval($values["tourney"]),
                    $values["player"]);
            }
        };
        $playerEntity = $this->playerManager->get($player);
        $name = $playerEntity[Model\PlayerManager::COLUMN_NAME];
        $surname = $playerEntity[Model\PlayerManager::COLUMN_SURNAME];
        $form->setDefaults(["player" => $name." ".$surname." (".$player.")"]);
        return $form;
    }

    public function playgroundsWithTourney($tourney){
        $playgrounds = $this->playgroudManager->getAll();
        $playgroundsWithTourney = array();
        foreach($playgrounds as $playground){
            if($playground[Model\PlaygroundManager::COLUMN_TOURNEY] == $tourney){
                array_push($playgroundsWithTourney, $playground[Model\PlaygroundManager::COLUMN_ID]);
            }
        }
        return $playgroundsWithTourney;
    }

	public function createPlaygroundsForm($playgrounds, $tourney){
	    $form = $this->factory->create();
        $playgroundsForTourney = array();
        foreach($playgrounds as $playground){
            if(empty($playground[Model\PlaygroundManager::COLUMN_TOURNEY]) ||
                $playground[Model\PlaygroundManager::COLUMN_TOURNEY] === $tourney){
                $playgroundID = $playground[Model\PlaygroundManager::COLUMN_ID];
                $playgroundsForTourney[$playgroundID] = $playgroundID;
            }
        }
        $form->addCheckboxList('playgrounds', "Hřiště", $playgroundsForTourney);
        $form->addHidden("tourney", $tourney);
        $form->addSubmit("save", "Uložit");
        $form->onSuccess[] = function(Form $form, $values){
            $this->setPlaygrounds($values["playgrounds"], $values["tourney"]);
        };
        return $form;
    }

    public function getTeams($id, $existing = true){
        $teams = $this->teamManager->getAll();
        $allTeams = array();
        foreach($teams as $team){
            $teamID = $team[Model\TeamManager::COLUMN_ID];
            $allTeams[$teamID] = $team[Model\TeamManager::COLUMN_TITLE]." (".$team[Model\TeamManager::COLUMN_ABBREVIATION].")";
        }
        $teamsAtTourney = $this->tourneyManagementManager->getTeamsByTourney($id);
        $teamsAtTourneyForForm = array();
        foreach($teamsAtTourney as $team){
            $teamID = $team[Model\TourneyManagementManager::COLUMN_TEAM];
            if($existing === false){
                unset($allTeams[$teamID]);
            } else {
                $teamsAtTourneyForForm[$teamID] = $allTeams[$teamID];
            }
        }
        return $existing === false ? $allTeams : $teamsAtTourneyForForm;
    }
    /**
     * @return Form
     */
	public function createAddTeamForm($id){
        $form = $this->factory->create();
        $form->addText("team", "Tým")->setRequired(true);
        $form->addInteger("seed", "Seed");
        $form->addHidden("id", $id);
        $form->addSubmit("addTeam", "Přidat tým");
        $form->onSuccess[] = [$this,"addTeamToTheTourney"];
        return $form;
    }

    public function addTeamToTheTourney(Form $form, $values){
        $startOfID = strrpos($values["team"], "(")+1;
        $endOfID = strrpos($values["team"], ")")-1;
        $values["team"] = intval(substr($values["team"], $startOfID, $endOfID));
        $this->addTeam($values);
        $categories = $this->categoryManager->getAll();
        $tourneyID = intval($values["id"]);
        $teamID = intval($values["team"]);
        $referredCategories = $this->refereeManager->getReferredCategoriesByTeam($tourneyID, $teamID);
        if($referredCategories->count() == 0){
            $team = $this->teamManager->get($teamID);
            $category = $team[Model\TeamManager::COLUMN_CATEGORY];
            foreach($categories as $secondCategory){
                $secondCategoryID = $secondCategory[Model\CategoryManager::COLUMN_ID];
                if($this->refereeManager->canBeARefereeOf($tourneyID, $category, $secondCategoryID)){
                    $this->refereeManager->addRefereeRule($tourneyID, $teamID, $secondCategoryID);
                }
            }
        }
    }

    public function getPlayers($tourney,$team){
        $tourney = $this->tourneyManager->get($tourney);
        $dateFrom = $tourney[Model\TourneyManager::COLUMN_FROM];
        $team = $this->teamManager->get($team);
        $categoryID = $team[Model\TeamManager::COLUMN_CATEGORY];
        $category = $this->categoryManager->get($categoryID);
        $sex = $category[Model\CategoryManager::COLUMN_SEX];
        $ageTo = $category[Model\CategoryManager::COLUMN_AGE_TO];
        $players = $this->playerManager->getSuitablePlayersForCategory($dateFrom, $sex, $ageTo);
        $allPlayers = array();
        foreach($players as $player){
            $playerID = $player[Model\PlayerManager::COLUMN_ID];
            $allPlayers[$playerID] = $player[Model\PlayerManager::COLUMN_NAME]." ".$player[Model\PlayerManager::COLUMN_SURNAME];
        }
        $playersAtTourney = $this->tourneyManagementManager->getPlayersByTeam($tourney);
        foreach ($playersAtTourney as $player){
            $playerID = $player[Model\TourneyManagementManager::SECOND_COLUMN_PLAYER];
            unset($allPlayers[$playerID]);
        }
        return $allPlayers;
    }
    /**
     * @return Form
     */
    public function createAddPlayerToTeamForm($tourney, $team){
        $form = $this->factory->create();
        $form->addHidden("team", $team);
        $form->addText("player", "Hráč")->setRequired(true);
        $form->addHidden("id", $tourney);
        $form->addSubmit("addPlayerToTheTeam", "Přidat hráče do týmu");
        $form->onSuccess[] = function(Form $form, $values){
            $startOfID = strrpos($values["player"],"(")+1;
            $endOfID = strrpos($values["player"],")")-1;
            $values["player"] = intval(substr($values["player"], $startOfID, $endOfID));
            $this->addPlayerToTheTeam($values);
        };
        return $form;
    }

    public function getCaptainOfTeam($tourney, $team){
        $teamEnt = null;
        $teamEntities = $this->tourneyManagementManager->getTeamByTourneyAndId($tourney, $team);
        foreach($teamEntities as $teamEntity){
            $teamEnt = $teamEntity;
        }
        return $teamEnt[Model\TourneyManagementManager::COLUMN_CAPTAIN];
    }

    public function getPlayersInTeam($tourney, $team){
        $playersInTeam = $this->tourneyManagementManager->getPlayersByTeam($tourney, $team);
        $players = array();
        foreach($playersInTeam as $playerInTeam){
            $playerID = $playerInTeam[Model\TourneyManagementManager::SECOND_COLUMN_PLAYER];
            $playerEntity = $this->playerManager->get($playerID);
            $name = $playerEntity[Model\PlayerManager::COLUMN_NAME]." ".
                $playerEntity[Model\PlayerManager::COLUMN_SURNAME];
            $players[$playerID] = $name;
        }
        return $players;
    }
    public function createChangeCaptainForm($tourney, $team){
        $form = $this->factory->create();
        $form->addSelect("captain", "Kapitán týmu");
        $form->addHidden("tourney", $tourney);
        $form->addHidden("team", $team);
        $form->addSubmit("changeCaptain", "Změnit kapitána týmu");
        $form["captain"]->setItems($this->getPlayersInTeam($tourney, $team));
        $form->setDefaults(["captain" => $this->getCaptainOfTeam($tourney, $team)]);
        $form->onSuccess[] = function(Form $form, $values){
            $tourney = intval($values["tourney"]);
            $team = intval($values["team"]);
            $player = intval($values["captain"]);
            $this->tourneyManagementManager->updateCaptain($tourney, $team, $player);
        };
        return $form;
    }

	/**
	 * @return Form
	 */
	public function create($edit, $id = null)
	{
		$form = $this->factory->create();
        $form->addText("title", "Název")->setRequired(true)->
        addRule(Form::MAX_LENGTH, "Název turnaje nesmí být delší než 70 znaků.",70);
        $form->addText("city", "Město")->setRequired(true)->
        addRule(Form::MIN_LENGTH,"Název města musí mít alespoň 2 znaky.",2);
        $form->addDatePicker("from", "Od")->setRequired(true);
        $form->addDatePicker("to", "Do")->setRequired(true);
        $form->addRadioList("rules","Pravidla pro rozhodování",
            ["eachTeamEachTeam" => "Každý tým může pískat libovolný jiný tým",
        "notAandBorLower" => "kategorie B a nižší nesmí pískat kategorii A",
        "onlyJuniors" => "kategorie B a nižší nesmí pískat kategorii A a junioři smí pískat pouze juniory",
                "customRules" => "Vlastní pravidla pro rozhodování"]);
        $form->addInteger("matchLength", "Délka zápasu:")->setRequired(true)->
        addRule(Form::MIN,"Délka zápasu musí být kladná.",1)->
        addRule(Form::MAX,"Délka zápasu nesmí být delší než 4 hodiny.",240);
        $form->addTextArea("description", "Popis");
        $form->addTextArea("program", "Program turnaje");
        if($edit === true){
            $form->addHidden("id", $id);
            $form->addSubmit("editTourney", "Upravit základní informace");
            $form->onValidate[] = function(Form $form, $values){
                if($this->matchManager->existsAnyMatchOutOfDateRange($values["id"],
                    $values["from"], $values["to"])){
                    $form->addError("Datum turnaje nemůže být změněno tak, aby se nějaký zápas konal mimo stanovený rozsah.");
                }
            };
            $form->onSuccess[] = function(Form $form, $values){
                $this->addOrEditTourney($values, true, $values["id"]);
            };
        } else {
            $form->addSubmit("addTourney", "Přidat turnaj");
            $form->onSuccess[] = function(Form $form, $values){
                $this->addOrEditTourney($values);
            };
        }
        $form->onValidate[] = function(Form $form, $values){
            if($values["from"] > $values["to"]){
                $form->addError("Datum zahájení turnaje nesmí být pozdější než datum konce turnaje.");
            }
        };
		return $form;
	}

    public function createFilterForm(){
        $form = $this->factory->create();
        $form->addText("title", "Název:");
        $form->addText("city", "Město:");
        $form->addDatePicker("from", "Od:");
        $form->addDatePicker("to", "Do:");
        $form->addSubmit("filterTourneys", "Vyhledat turnaje");
        $form->onValidate[] =  function(Form $form, $values){
            if(!empty($values["from"]) && !empty($values["to"]) &&
                $values["from"] > $values["to"]){
                $form->addError("Nejdřívější datum průběhu turnaje nesmí být pozdější než nejpozdější datum založení hřiště.");
            }
        };
        return $form;
    }

    public function createFilterMatchesForm(){
        $form = $this->factory->create();
        $hours = array();
        for($i = 0; $i < 24; $i++){
            $hour = $i < 10 ? "0".$i : "".$i;
            $hours[$hour] = $hour;
        }
        $minutes = array();
        for($i = 0; $i < 60; $i++){
            $minute = $i < 10 ? "0".$i : "".$i;
            $minutes[$minute] = $minute;
        }
        $form->addSelect("playground", "Hřiště:")->setAttribute('placeholder', '============');
        $form->addDatePicker("dateFrom","Čas od:");
        $form->addSelect("fromHour", "");
        $form["fromHour"]->setItems($hours);
        $form->addSelect("fromMinute", "");
        $form["fromMinute"]->setItems($minutes);
        $form->addDatePicker("dateTo", "Čas do:");
        $form->addSelect("toHour", "");
        $form["toHour"]->setItems($hours);
        $form->addSelect("toMinute", "");
        $form["toMinute"]->setItems($minutes);
        $form->addSelect("playingTeam", "Hrající tým:")->setAttribute('placeholder', '============');
        $form->addSelect("referee", "Rozhodčí tým:")->setAttribute('placeholder', '============');
        $form->addSubmit("filterMatches", "Vyhledat zápasy");
        return $form;
    }

    public function checkTourney(Form $form, $values){
        if($values["from"] > $values["to"]){
            $form->addError("Datum zahájení turnaje nesmí být pozdější než datum konce turnaje.");
        }
    }



	public function setPlaygrounds($values, $tourney){
	    $playgrounds = $this->playgroundManager->getAll();
        foreach($playgrounds as $playground){
            $tourneyID = $playground[Model\PlaygroundManager::COLUMN_TOURNEY];
            $playgroundID = $playground[Model\PlaygroundManager::COLUMN_ID];
            if(empty($tourneyID) && in_array($playgroundID, $values)) {
                $this->playgroundManager->setOrUnsetTourney($playgroundID, $tourney);
            } elseif($tourneyID == $tourney && !in_array($playgroundID, $values)){
                $this->playgroundManager->setOrUnsetTourney($playgroundID, $tourney, false);
            }
        }
    }
    /**
     * @param $values
     * @param bool $edit
     * @param null $id
     */
	public function addOrEditTourney($values, $edit = false, $id = null){
	    $title = $values["title"];
        $city = $values["city"];
        $from = new \DateTime($values["from"]);
        $to = new \DateTime($values["to"]);
        $matchLength = intval($values["matchLength"]);
        $description = $values["description"];
        $program = $values["program"];
        $rules = $values["rules"];
        if($edit == false){
            $this->tourneyManager->addTourney($title,$city, $from, $to, $matchLength, $description, $program, $rules);
        } else {
            $this->tourneyManager->editTourney($id,$title,$city, $from, $to, $matchLength, $description, $program);
        }
    }

    public function addTeam($values){
        $team = $values["team"];
        $seed = $values["seed"];
        $tourney = $values["id"];
        $this->tourneyManagementManager->addTeamWithSeed($tourney, $team, $seed);
    }
    public function addPlayerToTheTeam($values){
        $team = $values["team"];
        $player = $values["player"];
        $tourney = $values["id"];
        $this->tourneyManagementManager->addPlayerToTheTeam($player, $team, $tourney);
    }

    public function changeSeed($values){
        $tourney = intval($values["tourney"]);
        $team = intval($values["team"]);
        $seed = $values["seed"];
        $this->tourneyManagementManager->updateSeed($tourney, $team, $seed);
    }

    public function changePlayer($values){
        $newPlayer = $values["player"];
        $tourney = intval($values["tourney"]);
        $oldPlayer = $values["actualPlayer"];
        $teams = $this->tourneyManagementManager->getTeamByPlayerAndTourney($tourney, $oldPlayer);
        $teamID = null;
        foreach($teams as $team){
            $teamID = $team[Model\TourneyManagementManager::SECOND_COLUMN_TEAM];
        }
        $team = $this->tourneyManagementManager->getTeamByTourneyAndId($tourney, $teamID);
        foreach($team as $t){
            if($oldPlayer == $t[Model\TourneyManagementManager::COLUMN_CAPTAIN]){
                $this->tourneyManagementManager->updateCaptain($tourney, $teamID, $newPlayer);
            }
        }
        $this->tourneyManagementManager->replacePlayerWithDifferent($tourney, $oldPlayer, $newPlayer);
    }
}
