<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class BaseManager
{
    use Nette\SmartObject;

    /** @var Nette\Database\Context */
    protected $database;
    const TABLE_NAME = "Table",
        COLUMN_ID = "ID";

    /**
     * BaseManager constructor.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * @param $id
     * @return false|Nette\Database\Table\ActiveRow
     */
    public function get($id){
        return $this->database->table(static::TABLE_NAME)->get($id);

    }

    /**
     * @return mixed
     */
    public function getAll(){
        return $this->database->table(static::TABLE_NAME);
    }
    /**
     * @param $id
     */
    public function remove($id){
        $this->database->table(static::TABLE_NAME)->where(static::COLUMN_ID, $id)->delete();
    }
}