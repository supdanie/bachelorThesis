<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class CardManager extends BaseManager
{
    use Nette\SmartObject;

    const TABLE_NAME = 'Karta',
        COLUMN_ID = 'IDKarty',
        COLUMN_TYPE = 'Typ',
        COLUMN_TIME = 'Cas',
        COLUMN_PLAYER = 'IDHrace',
        COLUMN_MATCH = 'IDZapasu';

    public function addCard($type, $time, $player, $match){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $selection->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_TYPE => $type,
                self::COLUMN_TIME => $time,
                self::COLUMN_PLAYER => $player,
                self::COLUMN_MATCH => $match
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function editCard($id, $type, $time, $player, $matchID){
        $match = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($match)){
            $match->update([
                self::COLUMN_ID => $id,
                self::COLUMN_TYPE => $type,
                self::COLUMN_TIME => $time,
                self::COLUMN_PLAYER => $player,
                self::COLUMN_MATCH => $matchID
            ]);
        } else {
            throw new \Exception("Zápas s daným ID nebyl nalezen.");
        }
    }

    public function getCardsByMatch($match){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_MATCH, $match);
    }

    public function filterCards($match, $type, $timeFrom, $timeTo, $player, $limit = -1, $offset = -1){
        if(!empty($type) && substr_count($type, "c") === 0){
            return array();
        }
        $cards = $this->getCardsByMatch($match)->order(self::COLUMN_ID);
        $actualOffset = 0;
        $result = array();
        foreach($cards as $card){
            $addCard = true;
            if(!empty($timeFrom) && $card[self::COLUMN_TIME] < $timeFrom){
                $addCard = false;
            }
            if(!empty($timeTo) && $card[self::COLUMN_TIME] > $timeTo){
                $addCard = false;
            }
            if(!empty($player) && $player != -1 && $card[self::COLUMN_PLAYER] != $player){
                $addCard =  false;
            }
            if($addCard === true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($result, $card);
                } elseif($limit == -1 && $offset == -1) {
                    array_push($result, $card);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }


    public function hasPlayerGotARedCard($match, $player, $time){
        $redCards =  $this->database->table(self::TABLE_NAME)->where(self::COLUMN_MATCH, $match)
            ->where(self::COLUMN_PLAYER, $player)->where(self::COLUMN_TYPE, "r");
        foreach($redCards as $redCard){
            $timeOfCard = $redCard[self::COLUMN_TIME];
            if($timeOfCard <= $time){
                return true;
            }
        }
        return false;
    }

}