<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class CategoryManager extends BaseManager
{
    use Nette\SmartObject;

    const TABLE_NAME = 'Kategorie',
        COLUMN_ID = 'IDKategorie',
    COLUMN_TITLE = 'Oznaceni',
    COLUMN_SEX = 'Pohlavi',
    COLUMN_AGE_TO = 'Vek';

    public function addCategory($title, $sex, $ageTo){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_TITLE => $title,
                self::COLUMN_SEX => $sex,
                self::COLUMN_AGE_TO => empty($ageTo) ? NULL : $ageTo
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }


    /**
     * @param $value
     * @return array
     */
    public function getSexArrayFromValue($value){
        $array = array();
        if($value >= 4){
            array_push($array, "male");
        }
        if($value%4 >= 2){
            array_push($array, "female");
        }
        if($value%2 != 0){
            array_push($array, "unknown");
        }
        return $array;
    }


    /**
     * @param $id
     * @param $title
     * @param $sex
     * @param $ageTo
     */
    public function editCategory($id, $title, $sex, $ageTo){
        $category= $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($category)) {
            $category->update([
                self::COLUMN_ID => $id,
                self::COLUMN_TITLE => $title,
                self::COLUMN_SEX => $sex,
                self::COLUMN_AGE_TO => empty($ageTo) ? NULL : $ageTo
            ]);
        } else {
            throw new \Exception("Kategorie s daným ID nebyla nalezena.");
        }
    }

    public function filterCategoriesByParameters($title, $ageTo, $sex, $limit = -1, $offset = -1){
        $categories = array();
        $actualOffset = 0;
        $allCategories = $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
        foreach($allCategories as $category){
            $satisfactory = true;
            if(!empty($title) && strpos($category[self::COLUMN_TITLE], $title) === false){
                $satisfactory = false;
            }
            if(!empty($ageTo) && $category[self::COLUMN_AGE_TO] != $ageTo){
                $satisfactory = false;
            }
            if(!empty($sex) && $sex != -1 && $category[self::COLUMN_SEX] != $sex){
                $satisfactory = false;
            }
            if($satisfactory == true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($categories, $category);
                } elseif($limit < 0 && $offset < 0){
                    array_push($categories, $category);
                }
                $actualOffset++;
            }
        }
        return array_reverse($categories);
    }
}