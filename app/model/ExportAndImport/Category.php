<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:39
 */

namespace App\Model\ExportAndImport;


class Category
{
    private $id;
    private $title;
    private $numberOfGroups;
    private $categoriesToRefer;
    private $teams;
    public function __construct($id, $title, $numberOfGroups, $categoriesToRefer, $teams){
        $this->id = $id;
        $this->title = $title;
        $this->numberOfGroups = $numberOfGroups;
        $this->categoriesToRefer = $categoriesToRefer;
        $this->teams = $teams;
    }
    public function toJson(){
        $s = "{";
        $s.="\"id\" : \"".$this->id."\",";
        $s.="\"title\" : \"".$this->title."\",";
        $s.="\"numberOfGroups\" : ".$this->numberOfGroups.",";
        $s.="\"categoriesToRefer\" : [";
        $i = 0;
        foreach($this->categoriesToRefer as $category){
            if($i == count($this->categoriesToRefer) - 1){
                $s.=$category;
            } else {
                $s.=$category.",";
            }
            $i++;
        }
        $s.="],";
        $s.="\"teams\" : [";
        $i = 0;
        foreach($this->teams as $team){
            if($i == count($this->teams) - 1){
                $s.=$team->toJson();
            } else {
                $s.=$team->toJson().",";
            }
            $i++;
        }
        $s.="]}";
        return $s;
    }
}