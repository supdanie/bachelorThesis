<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:39
 */

namespace App\Model\ExportAndImport;


class Day
{
    private $day;
    private $month;
    private $year;
    public function __construct($day, $month, $year){
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
    }

    public function toJson(){
        $s="{ \"day\" :".$this->day.",";
        $s.="\"month\" :".$this->month.",";
        $s.="\"year\" :".$this->year."}";
        return $s;
    }
}