<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:39
 */

namespace App\Model\ExportAndImport;


class Playground
{
    private $number;
    private $playingDays;
    public function __construct($number, $playingDays){
        $this->number = $number;
        $this->playingDays = $playingDays;
    }
    public function toJson(){
        $s="{ \"number\" :".$this->number.",";
        $s.="\"playingDays\" :[";
        $i = 0;
        foreach($this->playingDays as $playingDay){
            if($i == count($this->playingDays) - 1){
                $s.=$playingDay->toJson();
            } else {
                $s.=$playingDay->toJson().",";
            }
            $i++;
        }
        $s.="]}";
        return $s;
    }
}