<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:39
 */

namespace App\Model\ExportAndImport;


class PlayingDay
{
    private $id;
    private $date;
    private $hourFrom;
    private $minuteFrom;
    private $hourTo;
    private $minuteTo;
    public function __construct($id, Day $date, $hourFrom, $minuteFrom, $hourTo, $minuteTo){
        $this->id = $id;
        $this->date = $date;
        $this->hourFrom = $hourFrom;
        $this->minuteFrom = $minuteFrom;
        $this->hourTo = $hourTo;
        $this->minuteTo = $minuteTo;
    }
    public function toJson(){
        $s="{ \"date\" :".$this->date->toJson().",";
        $s.="\"id\": ".$this->id.",";
        $s.="\"hourFrom\" :".$this->hourFrom.",";
        $s.="\"minuteFrom\" :".$this->minuteFrom.",";
        $s.="\"hourTo\" :".$this->hourTo.",";
        $s.="\"minuteTo\" :".$this->minuteTo."}";
        return $s;
    }
}