<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:38
 */

namespace App\Model\ExportAndImport;


class Team
{
    private $id;
    private $title;
    private $numberOfPlayers;
    public function __construct($id, $title, $numberOfPlayers){
        $this->id = $id;
        $this->title = $title;
        $this->numberOfPlayers = $numberOfPlayers;
    }
    public function toJson(){
        $s="{\"title\" : \"".$this->title."\",";
        $s.="\"id\": ".$this->id.",";
        $s.="\"numberOfPlayers\" :".$this->numberOfPlayers."}";
        return $s;
    }
}