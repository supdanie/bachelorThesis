<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 9:40
 */

namespace App\Model\ExportAndImport;


class Tourney
{
    private $from;
    private $to;
    private $categories;
    private $playgrounds;
    private $matchLength;
    public function __construct($categories, $playgrounds, $matchLength, Day $from, Day $to){
        $this->categories = $categories;
        $this->playgrounds = $playgrounds;
        $this->from = $from;
        $this->to = $to;
        $this->matchLength = $matchLength;
    }

    public function toJson(){
        $s="{ \"categories\": [";
        $i = 0;
        foreach($this->categories as $category){
            if($i == count($this->categories) - 1) {
                $s.=$category->toJson()."],";
            } else {
                $s.=$category->toJson().",";
            }
            $i++;
        }
        $s.="\"playgrounds\": [";
        $i=0;
        foreach($this->playgrounds as $playground){
            if($i == count($this->playgrounds) - 1) {
                $s.=$playground->toJson()."],";
            } else {
                $s.=$playground->toJson().",";
            }
            $i++;
        }
        $s.="\"matchLength\": ".$this->matchLength.",";
        $s.="\"from\": ".$this->from->toJson().",";
        $s.="\"to\": ".$this->to->toJson()."}";
        return $s;
    }
}