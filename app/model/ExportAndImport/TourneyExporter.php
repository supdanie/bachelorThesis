<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 10:03
 */

namespace App\Model\ExportAndImport;


use App\Model\CategoryManager;
use App\Model\MatchManager;
use App\Model\PlaygroundManager;
use App\Model\PlayingDayManager;
use App\Model\RefereeManager;
use App\Model\TeamManager;
use App\Model\TourneyManagementManager;
use App\Model\TourneyManager;
use Nette\Utils\DateTime;

class TourneyExporter
{
    private $tourneyManager;
    private $tourneyManagementManager;
    private $teamManager;
    private $categoryManager;
    private $playgroundManager;
    private $playingDayManager;
    private $refereeManager;
    public function __construct(TourneyManager $tourneyManager, TourneyManagementManager $tourneyManagementManager,
                                TeamManager $teamManager, CategoryManager $categoryManager, PlaygroundManager $playgroundManager,
                                PlayingDayManager $playingDayManager, RefereeManager $refereeManager){
        $this->tourneyManager = $tourneyManager;
        $this->tourneyManagementManager = $tourneyManagementManager;
        $this->teamManager = $teamManager;
        $this->categoryManager = $categoryManager;
        $this->playgroundManager = $playgroundManager;
        $this->playingDayManager = $playingDayManager;
        $this->refereeManager = $refereeManager;
    }

    public function exportTourney($id, $categoriesWithGroups){
        $tourney = $this->tourneyManager->get($id);
        $teams = $this->tourneyManagementManager->getTeamsByTourney($id);
        $allPlaygrounds = $this->playgroundManager->getAll()->where(PlaygroundManager::COLUMN_TOURNEY, $id);
        $categories = array();
        $playgrounds = array();
        foreach($teams as $team){
            $teamEntity = $this->teamManager->get($team[TourneyManagementManager::COLUMN_TEAM]);
            $categoryID = $teamEntity[TeamManager::COLUMN_CATEGORY];
            if(!array_key_exists($categoryID, $categories)){
                $categories[$categoryID] = $this->exportCategory($id, $categoryID, $categoriesWithGroups[$categoryID]);
            }
        }
        $from = $tourney[TourneyManager::COLUMN_FROM];
        $to = $tourney[TourneyManager::COLUMN_TO];
        foreach($allPlaygrounds as $playground){
            $playgroundID = $playground[PlaygroundManager::COLUMN_ID];
            array_push($playgrounds, $this->exportPlayground($playgroundID, $from, $to));
        }
        $matchLength = $tourney[TourneyManager::COLUMN_MATCH_LENGTH];
        $fromDay = intval($from->format("d"));
        $fromMonth = intval($from->format("m"));
        $fromYear = intval($from->format("Y"));
        $toDay = intval($to->format("d"));
        $toMonth = intval($to->format("m"));
        $toYear = intval($to->format("Y"));
        $fromDate = new Day($fromDay, $fromMonth, $fromYear);
        $toDate = new Day($toDay, $toMonth, $toYear);
        $exportedTourney = new Tourney($categories, $playgrounds, $matchLength, $fromDate, $toDate);
        return $exportedTourney;
    }

    public function exportPlayground($id, $from, $to){
        $playingDays = $this->playingDayManager->filterPlayingDays($id, $from, $to, true);
        $playingDaysForExport = array();
        foreach($playingDays as $playingDay){
            $date = $playingDay[PlayingDayManager::COLUMN_DAY];
            $id = $playingDay[PlayingDayManager::COLUMN_ID];
            $day = intval($date->format("d"));
            $month = intval($date->format("m"));
            $year = intval($date->format("Y"));
            $playingDayDate = new Day($day, $month, $year);
            $from = explode(':', $playingDay[PlayingDayManager::COLUMN_FROM]);
            $to = explode(':', $playingDay[PlayingDayManager::COLUMN_TO]);
            $playingDayForExport = new PlayingDay($id, $playingDayDate, intval($from[0]), intval($from[1]),
                intval($to[0]), intval($to[1]));
            array_push($playingDaysForExport, $playingDayForExport);
        }
        $playground = new Playground($id, $playingDaysForExport);
        return $playground;
    }

    public function exportTeam($tourney,$id){
        $numberOfPlayers = $this->tourneyManagementManager->getPlayersByTeam($tourney,$id)->count();;
        $teamEntity = $this->teamManager->get($id);
        $team = new Team($id, $teamEntity[TeamManager::COLUMN_ABBREVIATION], $numberOfPlayers);
        return $team;
    }

    public function exportCategory($tourney, $id, $numberOfGroups){
        $teamsOnTourney = $this->tourneyManagementManager->getTeamsByTourney($tourney);
        $teams = array();
        foreach($teamsOnTourney as $team){
            $teamEntity = $this->teamManager->get($team[TourneyManagementManager::COLUMN_TEAM]);
            if($teamEntity[TeamManager::COLUMN_CATEGORY] == $id){
                $exportedTeam = $this->exportTeam($tourney, $teamEntity[TeamManager::COLUMN_ID]);
                array_push($teams, $exportedTeam);
            }
        }
        $title = $this->categoryManager->get($id)[CategoryManager::COLUMN_TITLE];
        $categories = $this->categoryManager->getAll();
        $referredCategories = array();
        foreach($categories as $category){
            $categoryID = $category[CategoryManager::COLUMN_ID];
            if($this->refereeManager->canBeARefereeOf($tourney, $id, $categoryID) == true){
                array_push($referredCategories, $categoryID);
            }
        }

        $category = new Category($id, $title,$numberOfGroups, $referredCategories, $teams);
        return $category;
    }
}