<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.3.18
 * Time: 10:03
 */

namespace App\Model\ExportAndImport;


use App\Model\CategoryManager;
use App\Model\MatchManager;
use App\Model\PlaygroundManager;
use App\Model\PlayingDayManager;
use App\Model\TeamManager;
use App\Model\TourneyManagementManager;
use App\Model\TourneyManager;
use Nette\Utils\DateTime;

class TourneyImporter{
    private $matchManager;
    public function __construct(MatchManager $matchManager){
        $this->matchManager = $matchManager;
    }
    public function addMatches($parsedJson){
        $matchesArray = $parsedJson["matches"];
        foreach($matchesArray as $matchArray){
            $this->addMatch($matchArray);
        }
    }

    public function addMatch($matchArray){
        $hour = $matchArray["hourFrom"];
        $minute = $matchArray["minuteFrom"];
        $timeString = $hour.":".$minute;
        if($hour < 10 && $minute < 10){
            $timeString = "0".$hour.":0".$minute;
        } elseif($hour < 10){
            $timeString = "0".$hour.":".$minute;
        } elseif($minute < 10){
            $timeString = $hour.":0".$minute;
        }
        $group = $matchArray["group"];
        $playingDay = $matchArray["playingDay"];
        $firstTeam = $matchArray["firstTeam"];
        $secondTeam = $matchArray["secondTeam"];
        $referee = $matchArray["referee"];
        $this->matchManager->addMatch($timeString, $group, $playingDay, $firstTeam, $secondTeam, $referee);
    }
}