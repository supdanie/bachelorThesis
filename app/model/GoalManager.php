<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class GoalManager extends BaseManager
{
    use Nette\SmartObject;

    const TABLE_NAME = 'Gol',
        COLUMN_ID = 'IDGolu',
        COLUMN_TIME = 'Cas',
        COLUMN_PLAYER = 'IDHrace',
        COLUMN_MATCH = 'IDZapasu';

    private $tourneyManagementManager;
    public function __construct(Nette\Database\Context $database, TourneyManagementManager $tourneyManagementManager)
    {
        parent::__construct($database);
        $this->tourneyManagementManager = $tourneyManagementManager;
    }

    public function addGoal($time, $player, $match){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $selection->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_TIME => $time,
                self::COLUMN_PLAYER => $player,
                self::COLUMN_MATCH => $match
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function editGoal($id, $time, $player, $match){
        $goal = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($goal)) {
            $goal->update([
                self::COLUMN_ID => $id,
                self::COLUMN_TIME => $time,
                self::COLUMN_PLAYER => $player,
                self::COLUMN_MATCH => $match
            ]);
        } else {
            throw new \Exception("Gól s daným ID nebyl nalezen.");
        }
    }

    public function getGoalsByMatch($match){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_MATCH, $match);
    }

    public function filterGoals($match, $type, $timeFrom, $timeTo, $player, $limit = -1, $offset = -1){
        if(!empty($type) && substr_count($type, "g") === 0){
            return array();
        }
        $goals = $this->getGoalsByMatch($match)->order(self::COLUMN_ID);
        $result = array();
        $actualOffset = 0;
        foreach($goals as $goal){
            $addGoal = true;
            if(!empty($timeFrom) && $goal[self::COLUMN_TIME] < $timeFrom){
                $addGoal = false;
            }
            if(!empty($timeTo) && $goal[self::COLUMN_TIME] > $timeTo){
                $addGoal = false;
            }
            if(!empty($player) && $player != -1 && $goal[self::COLUMN_PLAYER] !=  $player){
                $addGoal = false;
            }
            if($addGoal === true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($result, $goal);
                } elseif($limit == -1 && $offset == -1) {
                    array_push($result, $goal);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }

    public function getGoalsByTeam($tourney, $match, $team){
        $goals = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_MATCH, $match);
        $players = $this->tourneyManagementManager->getPlayersByTeam($tourney, $team);
        $count = 0;
        foreach($players as $player){
            $playerID = $player[TourneyManagementManager::SECOND_COLUMN_PLAYER];
            $count +=$goals->where(self::COLUMN_PLAYER, $playerID)->count();
        }
        return $count;
    }
}