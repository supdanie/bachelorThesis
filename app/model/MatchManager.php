<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;



/**
 * Users management.
 */
class MatchManager extends BaseManager
{
	use Nette\SmartObject;
    const TABLE_NAME="Zapas",
        COLUMN_ID="IDZapasu",
        COLUMN_TIME="Cas",
        COLUMN_GROUP="Skupina",
        COLUMN_PLAYING_DAY="IDHracihoDne",
        COLUMN_FIRST_TEAM="PrvniTym",
        COLUMN_SECOND_TEAM="DruhyTym",
        COLUMN_REFEREE="Rozhodci";
    private $playingDayManager;
    private $playgroundManager;
    private $teamManager;
    public function __construct(Nette\Database\Context $database,
        PlayingDayManager $playingDayManager, PlaygroundManager $playgroundManager,
        TeamManager $teamManager)
    {
        parent::__construct($database);
        $this->playingDayManager = $playingDayManager;
        $this->playgroundManager = $playgroundManager;
        $this->teamManager = $teamManager;
    }

    public function addMatch($time, $group, $playingDay, $firstTeam, $secondTeam, $referee){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $selection->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_TIME => $time,
                self::COLUMN_GROUP => $group,
                self::COLUMN_PLAYING_DAY => $playingDay,
                self::COLUMN_FIRST_TEAM => $firstTeam,
                self::COLUMN_SECOND_TEAM => $secondTeam,
                self::COLUMN_REFEREE => $referee
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function isMatchAtTourney($playingDay, $id){
        $playingDay = $this->playingDayManager->get($playingDay);
        $playground = $this->playgroundManager->get($playingDay[PlayingDayManager::COLUMN_ID]);
        return $playground[PlaygroundManager::COLUMN_TOURNEY] == $id;
    }

    public function getDateByMatch($id){
        $match = $this->database->table(self::TABLE_NAME)->get($id);
        $playingDay = $this->playingDayManager->get($match[self::COLUMN_PLAYING_DAY]);
        return $playingDay[PlayingDayManager::COLUMN_DAY];
    }
    public function getPlaygroundByMatch($id){
        $match = $this->database->table(self::TABLE_NAME)->get($id);
        $playingDay = $this->playingDayManager->get($match[self::COLUMN_PLAYING_DAY]);
        return $playingDay[PlayingDayManager::COLUMN_PLAYGROUND];
    }

    public function getTeamsByMatch($id){
        $match = $this->database->table(self::TABLE_NAME)->get($id);
        $firstTeamID = $match[self::COLUMN_FIRST_TEAM];
        $firstTeam = $this->teamManager->get($firstTeamID)[TeamManager::COLUMN_ABBREVIATION];
        $secondTeamID = $match[self::COLUMN_SECOND_TEAM];
        $secondTeam = $this->teamManager->get($secondTeamID)[TeamManager::COLUMN_ABBREVIATION];
        $refereeID = $match[self::COLUMN_REFEREE];
        $referee = $this->teamManager->get($refereeID)[TeamManager::COLUMN_ABBREVIATION];
        return array($firstTeam, $secondTeam, $referee);
    }

    public function getMatchScheduleByTourney($id){
        $matches = $this->getMatchesByTourney($id);
        $matchSchedule = array();
        foreach($matches as $match){
            $teams = $this->getTeamsByMatch($match[self::COLUMN_ID]);
            $matchEntity = [self::COLUMN_ID => $match[self::COLUMN_ID],
            self::COLUMN_TIME => $match[self::COLUMN_TIME],
            self::COLUMN_GROUP => $match[self::COLUMN_GROUP],
            "date" => $this->getDateByMatch($match[self::COLUMN_ID]),
            "playground" => $this->getPlaygroundByMatch($match[self::COLUMN_ID]),
            "firstTeamID"=> $match[self::COLUMN_FIRST_TEAM],
            "secondTeamID"=> $match[self::COLUMN_SECOND_TEAM],
            "refereeID" => $match[self::COLUMN_REFEREE],
            self::COLUMN_FIRST_TEAM => $teams[0],
            self::COLUMN_SECOND_TEAM => $teams[1],
            self::COLUMN_REFEREE => $teams[2]];
            array_push($matchSchedule, $matchEntity);
        }
        return $matchSchedule;
    }

    public function existsAnyMatchOutOfDateRange($tourney, $from, $to){
        $matchSchedule = $this->getMatchScheduleByTourney($tourney);
        foreach($matchSchedule as $match){
            if($match["date"] < $from || $match["date"] > $to){
                return true;
            }
        }
        return false;
    }

    public function filterMatches($tourney, $playground, $dateFrom, $timeFrom, $dateTo, $timeTo,
                                  $playingTeam, $referee, $limit = -1, $offset = -1){
        $matches = $this->getMatchScheduleByTourney($tourney);
        $result = array();
        $actualOffset = 0;
        foreach($matches as $match){
            $addMatch = true;
            if($playground !== null && $playground != "any" && $match["playground"] != $playground){
                $addMatch = false;
            }
            if(!empty($dateFrom) && $match["date"] < $dateFrom){
                $addMatch = false;
            }
            if(!empty($dateFrom) && $match["date"] == $dateFrom
                && $timeFrom > $match[self::COLUMN_TIME]){
                $addMatch = false;
            }
            if(!empty($dateTo) && $match["date"] > $dateTo){
                $addMatch = false;
            }
            if(!empty($dateTo) && $match["date"] == $dateTo
                && $timeTo < $match[self::COLUMN_TIME]){
                $addMatch = false;
            }
            if($playingTeam !== null && $playingTeam != "any"  && $match["firstTeamID"] != $playingTeam &&
                $match["secondTeamID"] != $playingTeam){
                $addMatch = false;
            }
            if($referee !== null && $referee != "any" && $match["refereeID"] != $referee){
                $addMatch = false;
            }
            if($addMatch == true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit){
                    array_push($result, $match);
                } elseif($limit == -1 && $offset == -1) {
                    array_push($result, $match);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }

    /**
     * @param $id
     * @return array
     */
    public function getMatchesByTourney($id){
        $matches = array();
        $allMatches = $this->database->table(self::TABLE_NAME);
        foreach($allMatches as $match){
            if($this->isMatchAtTourney($match[MatchManager::COLUMN_PLAYING_DAY],$id)){
                array_push($matches, $match);
            }
        }
        return $matches;
    }

    /**
     * @param $id
     * @return bool
     */
    public function isMatchScheduleGenerated($id){
        return !empty($this->getMatchScheduleByTourney($id));
    }

}
