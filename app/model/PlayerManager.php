<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class PlayerManager extends BaseManager
{
	use Nette\SmartObject;

    const TABLE_NAME = 'Hrac',
        COLUMN_ID = 'IDHrace',
        COLUMN_NAME = 'Jmeno',
        COLUMN_SURNAME = 'Prijmeni',
        COLUMN_BIRTHDATE = 'DatumNarozeni',
        COLUMN_SEX = 'Pohlavi',
        COLUMN_NUMBER = 'Cislo';

    /**
     * @param $name
     * @param $surname
     * @param $birthdate
     * @param $sex
     * @param $number
     * @throws DuplicateNameException
     */
    public function addPlayer($name, $surname, $birthdate, $sex, $number){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_NAME => $name,
                self::COLUMN_SURNAME => $surname,
                self::COLUMN_BIRTHDATE => $birthdate,
                self::COLUMN_SEX => $sex,
                self::COLUMN_NUMBER => $number
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }
    /**
     * @param $id
     * @param $name
     * @param $surname
     * @param $birthdate
     * @param $sex
     * @param $number
     */
    public function editPlayer($id, $name, $surname, $birthdate, $sex, $number){
        $player = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($player)) {
            $player->update([
                self::COLUMN_ID => $id,
                self::COLUMN_NAME => $name,
                self::COLUMN_SURNAME => $surname,
                self::COLUMN_BIRTHDATE => $birthdate,
                self::COLUMN_SEX => $sex,
                self::COLUMN_NUMBER => $number
            ]);
        } else {
            throw new \Exception("Hráč s daným ID nebyl nalezen.");
        }
    }

    public function filterPlayers($name, $surname, $birthdateFrom, $birthdateTo, $sex, $number,
        $limit = -1, $offset = -1){
        $allPlayers = $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
        $players = array();
        $actualOffset = 0;
        foreach($allPlayers as $player){
            $satisfactory = true;
            if(!empty($name) && $player[self::COLUMN_NAME] != $name){
                $satisfactory = false;
            }
            if(!empty($surname) && $player[self::COLUMN_SURNAME] != $surname){
                $satisfactory = false;
            }
            if(!empty($birthdateFrom) && $player[self::COLUMN_BIRTHDATE] < $birthdateFrom){
                $satisfactory = false;
            }
            if(!empty($birthdateTo) && $player[self::COLUMN_BIRTHDATE] > $birthdateTo){
                $satisfactory = false;
            }
            if(!empty($sex) && !in_array($player[self::COLUMN_SEX], $sex)){
                $satisfactory = false;
            }
            if(!empty($number) && $player[self::COLUMN_NUMBER] != $number){
                $satisfactory = false;
            }
            if($satisfactory === true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($players, $player);
                } elseif($offset < 0 && $limit < 0){
                    array_push($players, $player);
                }
                $actualOffset++;
            }
        }
        return array_reverse($players);
    }

    public function getSuitablePlayersForCategory($date, $sex = 7, $ageTo = null){
        $birthdateFrom = null;
        if($ageTo != null){
            $year = intval($date->format("Y"))-$ageTo;
            $birthdateFrom = Nette\Utils\DateTime::fromParts($year,1,1);
        }
        $sexArray = [];
        if($sex >= 4){
            array_push($sexArray, "m");
        }
        if($sex%4 >= 2){
            array_push($sexArray, "f");
        }
        if($sex%2 == 1){
            array_push($sexArray, "u");
        }
        return $this->filterPlayers(null, null, $birthdateFrom, null, $sexArray, null);
    }
}
