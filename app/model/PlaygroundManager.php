<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class PlaygroundManager extends BaseManager
{
	use Nette\SmartObject;

    const TABLE_NAME = "Hriste",
        COLUMN_ID = "IDHriste",
        COLUMN_GPS = "GPS",
        COLUMN_COLOUR = "Barva",
        COLUMN_DATE_OF_FOUNDATION = "DatumZalozeni",
        COLUMN_DESCRIPTION="Popis",
        COLUMN_TOURNEY="turnajID";
    public function addPlayground($colour, $GPS, $dateOfFoundation, $description){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1,
                self::COLUMN_GPS => $GPS,
                self::COLUMN_COLOUR => $colour,
                self::COLUMN_DATE_OF_FOUNDATION => $dateOfFoundation,
                self::COLUMN_DESCRIPTION => $description
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }
    public function editPlayground($id, $colour, $GPS, $dateOfFoundation, $description){
        $playground = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($playground)) {
            $playground->update([
                self::COLUMN_ID => $id,
                self::COLUMN_GPS => $GPS,
                self::COLUMN_COLOUR => $colour,
                self::COLUMN_DATE_OF_FOUNDATION => $dateOfFoundation,
                self::COLUMN_DESCRIPTION => $description
            ]);
        } else {
            throw new \Exception("Hriste s danym ID nebylo nalezeno.");
        }
    }

    public function setOrUnsetTourney($id, $tourney, $set = true){
        $playground = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($playground)) {
            $playground->update([
                self::COLUMN_ID => $playground[self::COLUMN_ID],
                self::COLUMN_GPS => $playground[self::COLUMN_GPS],
                self::COLUMN_COLOUR => $playground[self::COLUMN_COLOUR],
                self::COLUMN_DATE_OF_FOUNDATION => $playground[self::COLUMN_DATE_OF_FOUNDATION],
                self::COLUMN_DESCRIPTION => $playground[self::COLUMN_DESCRIPTION],
                self::COLUMN_TOURNEY => $set === true ? $tourney : null
            ]);
        } else {
            throw new \Exception("Hriste s danym ID nebylo nalezeno.");
        }
    }
    public function playgroundsByTourney($id){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $id);
    }
    public function filterPlaygrounds($colour, $dateOfFoundationFrom, $dateOfFoundationTo,
        $limit = -1, $offset = -1){
        $playgrounds = $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
        $actualOffset = 0;
        $result = array();
        foreach($playgrounds as $playground){
            $addPlayground = true;
            if(!empty($colour) && $colour != "#000000" && $playground[self::COLUMN_COLOUR] != $colour){
                $addPlayground = false;
            }
            if(!empty($dateOfFoundationFrom) && $playground[self::COLUMN_DATE_OF_FOUNDATION] < $dateOfFoundationFrom){
                $addPlayground = false;
            }
            if(!empty($dateOfFoundationTo) && $playground[self::COLUMN_DATE_OF_FOUNDATION] > $dateOfFoundationTo){
                $addPlayground = false;
            }
            if($addPlayground == true){
                if($actualOffset >= $offset && $actualOffset < $offset + $limit) {
                    array_push($result, $playground);
                } elseif($limit == -1 && $offset == -1){
                    array_push($result, $playground);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }
}
