<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class PlayingDayManager extends BaseManager
{
	use Nette\SmartObject;
    const TABLE_NAME = 'HraciDen',
        COLUMN_ID = "IDHracihoDne",
        COLUMN_DAY = 'Datum',
        COLUMN_FROM = 'CasOd',
        COLUMN_TO = 'CasDo',
        COLUMN_PLAYGROUND = 'IDHriste';

    /**
     * @param $day
     * @param $from
     * @param $to
     * @param $playground
     * @throws DuplicateNameException
     */
    public function addPlayingDay($day, $from, $to, $playground){
        try {
            $selection = $this->database->table(self::TABLE_NAME);
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID) + 1,
                self::COLUMN_DAY => $day,
                self::COLUMN_FROM => $from,
                self::COLUMN_TO => $to,
                self::COLUMN_PLAYGROUND => $playground
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function getPlayingDaysAtPlayground($id){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_PLAYGROUND,$id);
    }


    public function editPlayingDay($id, $day, $from, $to, $playground){
        $playingDay = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($playingDay)){
            throw new \Exception("Nelze najít žádný hrací den s tímto ID.");
        } elseif ($playingDay[self::COLUMN_PLAYGROUND] != $playground){
            throw new \Exception("Hrací den s tímto ID nelze najít na daném hřišti.");
        } else{
            $playingDay->update([
                self::COLUMN_ID => $id,
                self::COLUMN_DAY => $day,
                self::COLUMN_FROM => $from,
                self::COLUMN_TO => $to,
                self::COLUMN_PLAYGROUND => $playground
            ]);
        }
    }

    public function existsPlayingDayAtThisTime($playground, $day, $from, $to, $id = null){
        $playingDays = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_PLAYGROUND, $playground)
            ->where(self::COLUMN_DAY, $day);
        foreach($playingDays as $playingDay){
            if($to <= $playingDay[self::COLUMN_FROM] || $from >= $playingDay[self::COLUMN_TO]){
                continue;
            } else {
                if($playingDay[self::COLUMN_ID] === $id){
                    continue;
                }
                return true;
            }
        }
        return false;
    }

    public function filterPlayingDays($playground, $from, $to, $order = false, $limit = -1, $offset = -1){
        $playingDays = $this->database->table(self::TABLE_NAME)->
        where(self::COLUMN_PLAYGROUND, $playground);
        if($order == true){
            $playingDays = $playingDays->order(self::COLUMN_DAY);
        } else {
            $playingDays = $playingDays->order(self::COLUMN_ID);
        }
        $result = array();
        $actualOffset = 0;
        foreach($playingDays as $playingDay) {
            $addPlayingDay = true;
            if (empty($from) && !empty($to)) {
                $addPlayingDay = $playingDay[self::COLUMN_DAY] <= $to;
            } elseif (empty($to) && !empty($from)) {
                $addPlayingDay = $playingDay[self::COLUMN_DAY] >= $from;
            } elseif(!empty($from) && !empty($to)) {
                $addPlayingDay = $playingDay[self::COLUMN_DAY] >= $from &&
                    $playingDay[self::COLUMN_DAY] <= $to;
            }
            if($addPlayingDay == true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($result, $playingDay);
                } elseif ($limit == -1 && $offset == -1){
                    array_push($result, $playingDay);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }
}
