<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class RefereeManager extends BaseManager
{
	use Nette\SmartObject;

    const TABLE_NAME="Rozhoduje",
        COLUMN_TEAM="IDTymu",
        COLUMN_CATEGORY="IDKategorie",
        COLUMN_TOURNEY="IDTurnaje";

    /** @var TeamManager */
    private $teamManager;
    /** @var CategoryManager */
    private $categoryManager;
    public function __construct(Nette\Database\Context $database, TeamManager $teamManager, CategoryManager $categoryManager)
    {
        parent::__construct($database);
        $this->teamManager = $teamManager;
        $this->categoryManager = $categoryManager;
    }

    /**
     * @param $tourney
     * @param $team
     * @param $category
     * @throws DuplicateNameException
     */
    public function addRefereeRule($tourney, $team, $category){
        try {
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_TOURNEY => $tourney,
                self::COLUMN_TEAM => $team,
                self::COLUMN_CATEGORY => $category
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }


    public function addRefereeRuleOnlyJuniors($categoryOfTeam, $categoryTitle, $tourney, $team, $category){
        if(mb_substr($categoryOfTeam, 0, 7) == "Junioři" && mb_substr($categoryTitle,0,7) == "Junioři"){
            $this->addRefereeRule($tourney, $team, $category);
        } elseif(mb_substr($categoryOfTeam, -1, 1) == "A" || mb_substr($categoryOfTeam, -2, 2) == " a"){
            $this->addRefereeRule($tourney, $team, $category);
        } elseif(mb_substr($categoryTitle, -1, 1) != "A" && mb_substr($categoryTitle, -2, 2) != " a" &&
            mb_substr($categoryOfTeam, 0, 7) != "Junioři"){
            $this->addRefereeRule($tourney, $team, $category);
        }
    }

    public function addRefereeRuleNotAAndBOrLower($categoryOfTeam, $categoryTitle, $tourney, $team, $category){
        if(mb_substr($categoryOfTeam, -1, 1) == "A" || mb_substr($categoryOfTeam, -2, 2) == " a"){
            $this->addRefereeRule($tourney, $team, $category);
        } elseif(mb_substr($categoryTitle, -1, 1) != "A" && mb_substr($categoryTitle, -2, 2) != " a"){
            $this->addRefereeRule($tourney, $team, $category);
        }
    }
    public function addRefereeRules($rules, $tourney){
        $teams = $this->teamManager->getAll();
        $categories = $this->categoryManager->getAll();
        if($rules == "eachTeamEachTeam"){
            foreach($teams as $team){
                foreach($categories as $category){
                    $this->addRefereeRule($tourney, $team, $category);
                }
            }
        } elseif($rules == "onlyJuniors"){
            foreach($teams as $team){
                $categoryOfTeam = $this->categoryManager->get($team[TeamManager::COLUMN_CATEGORY])[CategoryManager::COLUMN_TITLE];
                foreach($categories as $category){
                    $categoryTitle = $category[CategoryManager::COLUMN_TITLE];
                    $this->addRefereeRuleOnlyJuniors($categoryOfTeam, $categoryTitle, $tourney, $team, $category);
                }
            }
        } elseif($rules == "notAandBorLower") {
            foreach($teams as $team){
                $categoryOfTeam = $this->categoryManager->get($team[TeamManager::COLUMN_CATEGORY])[CategoryManager::COLUMN_TITLE];
                foreach($categories as $category){
                    $categoryTitle = $category[CategoryManager::COLUMN_TITLE];
                    $this->addRefereeRuleNotAAndBOrLower($categoryOfTeam, $categoryTitle, $tourney, $team, $category);
                }
            }
        }
    }


    /**
     * @param $tourney
     * @param $team
     * @param $category
     */
    public function removeRefereeRule($tourney,$team,$category){
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->where(self::COLUMN_TEAM, $team)->
        where(self::COLUMN_CATEGORY, $category)->delete();
    }


    public function removeRefereeRulesByTourney($tourney){
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->delete();
    }

    public function canBeARefereeOf($tourney, $firstCategory, $secondCategory){
        $canRefereeSecondCategory = $this->database->table(self::TABLE_NAME)->
        where(self::COLUMN_CATEGORY, $secondCategory)->where(self::COLUMN_TOURNEY, $tourney);
        foreach($canRefereeSecondCategory as $referee){
            $teamID = $referee[self::COLUMN_TEAM];
            $team = $this->teamManager->get($teamID);
            if($team[TeamManager::COLUMN_CATEGORY] == $firstCategory){
                return true;
            }
        }
        return false;
    }

    public function areSetRulesForTourney($tourney){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->count() >= 1;
    }

    public function canRefereeOnlyJuniors($firstCategory, $secondCategory){
        $firstCategoryTitle = $this->categoryManager->get($firstCategory)[CategoryManager::COLUMN_TITLE];
        $secondCategoryTitle = $this->categoryManager->get($secondCategory)[CategoryManager::COLUMN_TITLE];
        if(mb_substr($firstCategoryTitle, 0, 7) == "Junioři" && mb_substr($secondCategoryTitle,0,7) == "Junioři"){
            return true;
        } elseif(mb_substr($firstCategoryTitle, -1, 1) == "A" || mb_substr($firstCategoryTitle, -2, 2) == " a"){
            return true;
        } elseif(mb_substr($secondCategoryTitle, -1, 1) != "A" && mb_substr($secondCategoryTitle, -2, 2) != " a" &&
            mb_substr($firstCategoryTitle, 0, 7) != "Junioři"){
            return true;
        }
        return false;
    }

    public function canRefereeNotAAndBOrLower($firstCategory, $secondCategory){
        $firstCategoryTitle = $this->categoryManager->get($firstCategory)[CategoryManager::COLUMN_TITLE];
        $secondCategoryTitle = $this->categoryManager->get($secondCategory)[CategoryManager::COLUMN_TITLE];
        if(mb_substr($firstCategoryTitle, -1, 1) == "A" || mb_substr($firstCategoryTitle, -2, 2) == " a"){
            return true;
        } elseif(mb_substr($secondCategoryTitle, -1, 1) != "A" && mb_substr($secondCategoryTitle, -2, 2) != " a"){
            return true;
        }
        return false;
    }

    public function canRefer($tourney){
        $firstCategories = $this->categoryManager->getAll();
        $secondCategories = $this->categoryManager->getAll();
        $eachTeamEachTeam = true;
        $notAandBorLower = true;
        $onlyJuniors = true;
        foreach($firstCategories as $firstCategory){
            foreach ($secondCategories as $secondCategory){
                $firstCategoryID = $firstCategory[CategoryManager::COLUMN_ID];
                $secondCategoryID = $secondCategory[CategoryManager::COLUMN_ID];
                if($this->canBeARefereeOf($tourney, $firstCategoryID, $secondCategoryID) == false){
                    $eachTeamEachTeam = false;
                }
                if($this->canBeARefereeOf($tourney, $firstCategoryID, $secondCategoryID)
                    != $this->canRefereeNotAAndBOrLower($firstCategoryID,$secondCategoryID)){
                    $notAandBorLower = false;
                }
                if($this->canBeARefereeOf($tourney, $firstCategoryID, $secondCategoryID)
                    != $this->canRefereeOnlyJuniors($firstCategoryID,$secondCategoryID)){
                    $onlyJuniors = false;
                }
            }
        }
        if($eachTeamEachTeam == true){
            return "eachTeamEachTeam";
        }
        if($notAandBorLower == true){
            return "notAandBorLower";
        }
        if($onlyJuniors == true){
            return "onlyJuniors";
        }
        return "customRules";
    }


    public function getReferredCategoriesByTeam($tourney, $team){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)
            ->where(self::COLUMN_TEAM, $team);
    }
}
