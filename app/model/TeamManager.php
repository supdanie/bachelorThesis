<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class TeamManager extends BaseManager
{
	use Nette\SmartObject;
    const TABLE_NAME = 'Tym',
        COLUMN_ID = "IDTymu",
        COLUMN_TITLE = 'Nazev',
        COLUMN_ABBREVIATION = 'Zkratka',
        COLUMN_CATEGORY = 'IDKategorie',
        COLUMN_PRIMARY_COLOR = 'Barva1',
        COLUMN_SECONDARY_COLOR = 'Barva2';

    /**
     * @param $title
     * @param $abbreviation
     * @param $category
     * @param $primaryColour
     * @param $secondaryColour
     * @throws DuplicateNameException
     */
    public function addTeam($title, $abbreviation, $category, $primaryColour, $secondaryColour){
        try {
            $selection = $this->database->table(self::TABLE_NAME);
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID) + 1,
                self::COLUMN_TITLE => $title,
                self::COLUMN_ABBREVIATION => $abbreviation,
                self::COLUMN_CATEGORY => $category,
                self::COLUMN_PRIMARY_COLOR => $primaryColour,
                self::COLUMN_SECONDARY_COLOR => $secondaryColour
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    /**
     * @param $id
     * @return static
     */
    public function getTeamsInCategory($id){
        $selection = $this->database->table(self::TABLE_NAME);
        return $selection->where(self::COLUMN_CATEGORY, $id);
    }


    /**
     * @param $id
     * @param $title
     * @param $abbreviation
     * @param $category
     * @param $primaryColour
     * @param $secondaryColour
     */
    public function editTeam($id, $title, $abbreviation, $category, $primaryColour, $secondaryColour){
        $team = $this->database->table(self::TABLE_NAME)->get($id);
        if(!empty($team)) {
            $team->update([
                self::COLUMN_ID => $id,
                self::COLUMN_TITLE => $title,
                self::COLUMN_ABBREVIATION => $abbreviation,
                self::COLUMN_CATEGORY => $category,
                self::COLUMN_PRIMARY_COLOR => $primaryColour,
                self::COLUMN_SECONDARY_COLOR => $secondaryColour
            ]);
        } else {
            throw new \Exception("Tým s daným ID nebyl nalezen.");
        }
    }

    /**
     * @param $title
     * @param $abbreviation
     * @param $category
     * @param $primaryColour
     * @param $secondaryColour
     * @return array
     */
    public function filterTeamsByParameters($title, $abbreviation, $category, $primaryColour,
                                            $secondaryColour, $limit = -1, $offset = -1){
        $allTeams = $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
        $teams = array();
        $actualOffset = 0;
        foreach($allTeams as $team){
            $satisfactory = true;
            if(!empty($title) && $team[self::COLUMN_TITLE] != $title){
                $satisfactory = false;
            }
            if(!empty($abbreviation) && $team[self::COLUMN_ABBREVIATION] != $abbreviation){
                $satisfactory = false;
            }
            if($category != -1 && $team[self::COLUMN_CATEGORY] != $category){
                $satisfactory = false;
            }
            if(!empty($primaryColour) && $primaryColour != "#000000"
                && $team[self::COLUMN_PRIMARY_COLOR] != $primaryColour){
                $satisfactory = false;
            }
            if(!empty($secondaryColour) && $secondaryColour != "#000000"
                && $team[self::COLUMN_SECONDARY_COLOR] != $secondaryColour){
                $satisfactory = false;
            }
            if($satisfactory == true){
                if($actualOffset >= $offset && $actualOffset < $offset+$limit) {
                    array_push($teams, $team);
                } elseif($offset < 0 && $limit < 0){
                    array_push($teams, $team);
                }
                $actualOffset++;
            }
        }
        return array_reverse($teams);
    }
}
