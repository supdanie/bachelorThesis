<?php

namespace App\Model;

use h4kuna\Assets\DuplicityAssetNameException;
use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class TourneyManagementManager
{
	use Nette\SmartObject;

    /** @var Nette\Database\Context */
    protected $database;
    protected $teamManager;

    const TABLE_NAME="Ucast",
        COLUMN_TEAM="IDTymu",
        COLUMN_SEED="Seed",
        COLUMN_TOURNEY="IDTurnaje",
        COLUMN_CAPTAIN="hracID";

    const SECOND_TABLE_NAME = "JeVSestave",
        SECOND_COLUMN_PLAYER = "IDHrace",
        SECOND_COLUMN_TOURNEY = "IDTurnaje",
        SECOND_COLUMN_TEAM = "IDTymu";

    /**
     * BaseManager constructor.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database, TeamManager $teamManager)
    {
        $this->database = $database;
        $this->teamManager = $teamManager;
    }
    public function addTeamWithSeed($tourney, $team, $seed){
        try {
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_SEED => $seed,
                self::COLUMN_TOURNEY => $tourney,
                self::COLUMN_TEAM => $team
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function updateSeed($tourney, $team, $seed){
        $rows = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->where(self::COLUMN_TEAM, $team);
        foreach($rows as $row){
            $row->update([
                self::COLUMN_SEED => $seed,
                self::COLUMN_TOURNEY => $tourney,
                self::COLUMN_TEAM => $team
            ]);
        }
    }

    public function updateCaptain($tourney, $team, $captain){
        $rows = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->where(self::COLUMN_TEAM, $team);
        foreach($rows as $row){
            $row->update([
                self::COLUMN_SEED => $row[self::COLUMN_SEED],
                self::COLUMN_TOURNEY => $tourney,
                self::COLUMN_TEAM => $team,
                self::COLUMN_CAPTAIN => $captain
            ]);
        }
    }

    public function addPlayerToTheTeam($player, $team, $tourney){
        try {
            if($this->getPlayersByTeam($tourney, $team)->count() == 0){
                $this->updateCaptain($tourney, $team, $player);
            }
            if($this->checkWhetherPlayerIsNotInATeam($tourney, $player)) {
                $this->database->table(self::SECOND_TABLE_NAME)->insert([
                    self::SECOND_COLUMN_TOURNEY => $tourney,
                    self::SECOND_COLUMN_TEAM => $team,
                    self::SECOND_COLUMN_PLAYER => $player
                ]);
            } else {
                throw new DuplicityAssetNameException("Nelze vložit hráče do dvou různých týmů");
            }
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function removeTeamsFromTourney($tourney){
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)->delete();
    }


    public function getTeamsByTourneySortedBySeedAndID($tourney){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)
            ->order(self::COLUMN_SEED)->order(self::COLUMN_TEAM);
    }

    public function getTeamsByTourney($tourney){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney);
    }
    public function getPlayersByTeam($tourney, $team = null){
        $playersAtTourney = $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney);
        if($team == null){
            return $playersAtTourney;
        }
        return $playersAtTourney->where(self::SECOND_COLUMN_TEAM, $team);
    }

    public function removePlayersFromTourney($tourney){
        $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)->delete();
    }

    public function removePlayerFromTourney($tourney, $player){
        $team = null;
        $players = $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)
            ->where(self::SECOND_COLUMN_PLAYER, $player);
        foreach($players as $player){
            $team = $player[self::SECOND_COLUMN_TEAM];
        }
        $players->delete();
        $playersInTeam = $this->getPlayersByTeam($tourney, $team);
        $captain = null;
        foreach($playersInTeam as $player){
            $captain = $player[self::SECOND_COLUMN_PLAYER];
        }
        $this->updateCaptain($tourney, $team, $captain);
    }

    public function removeTeamFromTourney($tourney, $team){
        $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)
            ->where(self::SECOND_COLUMN_TEAM, $team)->delete();

        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)
            ->where(self::COLUMN_TEAM, $team)->delete();
    }

    public function replacePlayerWithDifferent($tourney, $player, $newPlayer){
        $teamsWithPlayer = $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)->
        where(self::SECOND_COLUMN_PLAYER, $player);
        foreach($teamsWithPlayer as $team) {
            $teamsWithPlayer->delete();
            $this->addPlayerToTheTeam($newPlayer, $team[self::SECOND_COLUMN_TEAM], $tourney);
        }
    }
    public function checkWhetherPlayerIsNotInATeam($tourney, $player){
        return $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)->
        where(self::SECOND_COLUMN_PLAYER, $player)->count() == 0;
    }


    public function getTeamByPlayerAndTourney($tourney, $player){
        return $this->database->table(self::SECOND_TABLE_NAME)->where(self::SECOND_COLUMN_TOURNEY, $tourney)->
        where(self::SECOND_COLUMN_PLAYER, $player);
    }

    public function getTeamByTourneyAndId($tourney, $team){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOURNEY, $tourney)
            ->where(self::COLUMN_TEAM, $team);
    }

    public function existsAnyTeamInCategoryAtTourney($tourney, $category){
        $teams = $this->getTeamsByTourney($tourney);
        foreach($teams as $team){
            $teamID = $team[self::COLUMN_TEAM];
            $teamEntity = $this->teamManager->get($teamID);
            $teamCategory = $teamEntity[TeamManager::COLUMN_CATEGORY];
            if($teamCategory == $category){
                return true;
            }
        }
        return false;
    }
    
}
