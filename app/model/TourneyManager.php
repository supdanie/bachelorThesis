<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;



/**
 * Users management.
 */
class TourneyManager extends BaseManager
{
	use Nette\SmartObject;

    const TABLE_NAME="Turnaj",
        COLUMN_ID="IDTurnaje",
        COLUMN_TITLE="Nazev",
        COLUMN_CITY="Mesto",
        COLUMN_FROM="DatumOd",
        COLUMN_TO="DatumDo",
        COLUMN_DESCRIPTION = "Popis",
        COLUMN_PROGRAM = "ProgramTurnaje",
        COLUMN_MATCH_LENGTH="DelkaZapasu";
    /** @var RefereeManager */
    private $refereeManager;
    public function __construct(Nette\Database\Context $database, RefereeManager $refereeManager)
    {
        parent::__construct($database);
        $this->refereeManager = $refereeManager;
    }

    public function addTourney($title, $city, $from, $to, $matchLength, $description, $program, $rules){
        $selection = $this->database->table(self::TABLE_NAME);
        try {
            $id = $selection->count() == 0 ? 0 : $selection->max(self::COLUMN_ID)+1;
            $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_ID => $id,
                self::COLUMN_TITLE => $title,
                self::COLUMN_CITY => $city,
                self::COLUMN_FROM => $from,
                self::COLUMN_TO => $to,
                self::COLUMN_DESCRIPTION => $description,
                self::COLUMN_PROGRAM => $program,
                self::COLUMN_MATCH_LENGTH => $matchLength
            ]);
            $this->refereeManager->addRefereeRules($rules, $id);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    /*
     * @param $id
     * @param $title
     * @param $from
     * @param $to
     * @throws DuplicateNameException
     */
    public function editTourney($id ,$title, $city, $from, $to, $matchLength, $description, $program){
        $tourney = $this->database->table(self::TABLE_NAME)->get($id);
        if (!empty($tourney)) {
            $tourney->update([
                self::COLUMN_ID => $id,
                self::COLUMN_TITLE => $title,
                self::COLUMN_CITY => $city,
                self::COLUMN_FROM => $from,
                self::COLUMN_TO => $to,
                self::COLUMN_DESCRIPTION => $description,
                self::COLUMN_PROGRAM => $program,
                self::COLUMN_MATCH_LENGTH => $matchLength
            ]);
        } else {
            throw new \Exception("Turnaj s daným ID nebyl nalezen.");
        }
    }

    public function filterTourneys($title, $city, $from, $to, $limit = -1, $offset = -1){
        $tourneys = $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
        $actualOffset = 0;
        $result = array();
        foreach($tourneys as $tourney){
            $addTourney = true;
            if(!empty($title) && strpos($tourney[self::COLUMN_TITLE], $title) === false){
                $addTourney = false;
            }
            if(!empty($city) && $tourney[self::COLUMN_CITY] != $city){
                $addTourney = false;
            }
            if(!empty($from) && $tourney[self::COLUMN_TO] < $from){
                $addTourney = false;
            }
            if(!empty($to) && $tourney[self::COLUMN_FROM] > $to){
                $addTourney = false;
            }
            if($addTourney == true){
                if($actualOffset >= $offset && $actualOffset < $offset + $limit) {
                    array_push($result, $tourney);
                } elseif($limit == -1 && $offset == -1){
                    array_push($result, $tourney);
                }
                $actualOffset++;
            }
        }
        return array_reverse($result);
    }

    public function getMaximumID(){
        return $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID);
    }
}
