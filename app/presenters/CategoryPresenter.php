<?php

namespace App\Presenters;

use App\Forms;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Utils\Paginator;


class CategoryPresenter extends BasePresenter
{
    /** @var Forms\CategoryFormFactory */
    private $categoryFactory;
    private $categoryManager;
    private $editedCategory;
    /** @persistent */
    public $title;
    /** @persistent */
    public $ageTo;
    /** @persistent */
    public $sex;
    /**
     * CategoryPresenter constructor.
     * @param Forms\CategoryFormFactory $categoryFactory
     */
    public function __construct(Forms\CategoryFormFactory $categoryFactory,
        Model\CategoryManager $categoryManager){
        $this->categoryFactory = $categoryFactory;
        $this->categoryManager = $categoryManager;
        $this->title = null;
        $this->ageTo = null;
        $this->sex = null;
    }

    /**
     * @param $id
     */
    public function actionDelete($id){
        $this->categoryManager->remove($id);
        return $this->redirect(303, "Category:default");
    }

    public function actionEdit($id){
        $this->editedCategory = $id;
        $form = $this["editForm"];
        $form->onSuccess[] = function (Form $form, $values){
            $this->categoryFactory->addOrEditCategory($form, $values, true, $values["id"]);
            $this->redirect("default");
        };
    }
    /**
     * Sign-up form factory.
     * @return Form
     */
    public function createComponentAddForm()
    {
        return $this->categoryFactory->create(false);
    }

    public function createComponentEditForm(){
        $id = intval($this->editedCategory);
        $category = $this->categoryManager->get($id);
        $array = $category->toArray();
        $sex = $array[Model\CategoryManager::COLUMN_SEX];
        $form = $this->categoryFactory->create(true, $id)->setDefaults(
            ["title" => $array[Model\CategoryManager::COLUMN_TITLE],
                "sex" => $this->categoryManager->getSexArrayFromValue(intval($sex)),
                "ageTo" => $array[Model\CategoryManager::COLUMN_AGE_TO]]
        );
        return $form;
    }
    /**
     * @return Form
     */
    public function createComponentFilterForm(){
        $form =  $this->categoryFactory->createFilterForm();
        $form->onSuccess[] = [$this, "filterCategories"];
        return $form;
    }

    public function filterCategories(Form $form, $values){
        $sex = 0;
        if(!empty($values["sex"])) {
            if (array_search("male", $values["sex"]) !== false) {
                $sex += 4;
            }
            if (array_search("female", $values["sex"]) !== false) {
                $sex += 2;
            }
            if (array_search("unknown", $values["sex"]) !== false) {
                $sex += 1;
            }
        }
        $this->title = $values["title"];
        $this->ageTo = $values["ageTo"];
        $this->sex = empty($values["sex"]) ? -1 :$sex;
    }



    /**
     *
     */
    public function renderDefault($page = 1)
    {
        $categories = $this->categoryManager->filterCategoriesByParameters($this->title, $this->ageTo, $this->sex);
        $paginator = new Paginator();
        $paginator->setItemCount(count($categories));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->categories = $this->categoryManager->
        filterCategoriesByParameters($this->title, $this->ageTo, $this->sex, $paginator->getLength(), $paginator->getOffset());
        $this->template->title = Model\CategoryManager::COLUMN_TITLE;
        $this->template->sex = Model\CategoryManager::COLUMN_SEX;
        $this->template->id = Model\CategoryManager::COLUMN_ID;
        $this->template->ageTo = Model\CategoryManager::COLUMN_AGE_TO;
    }

    public function renderEdit($id){
        $this->template->category = $this->categoryManager->get($id);
        $this->template->id = Model\CategoryManager::COLUMN_ID;
    }
}
