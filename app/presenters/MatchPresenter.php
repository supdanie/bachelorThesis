<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Utils\ArrayHash;
use Nette\Utils\Paginator;

class MatchPresenter extends BasePresenter
{
    private $goalFormFactory;
    private $goalManager;
    private $cardFormFactory;
    private $cardManager;
    private $gameEventFormFactory;
    private $matchManager;
    private $tourneyManagementManager;
    private $tourneyManager;
    private $teamManager;
    private $playerManager;
    private $tourney;
    private $match;
    private $editedGoal;
    private $editedCard;
    /** @persistent */
    public $type;
    /** @persistent */
    public $timeFrom;
    /** @persistent */
    public $timeTo;
    /** @persistent */
    public $player;
    public function __construct(Forms\GoalFormFactory $goalFormFactory, Model\GoalManager $goalManager,
    Forms\CardFormFactory $cardFormFactory, Model\CardManager $cardManager, Model\TourneyManager $tourneyManager,
    Model\MatchManager $matchManager, Model\TourneyManagementManager $tourneyManagementManager,
    Model\TeamManager $teamManager, Model\PlayerManager $playerManager,
    Forms\GameEventFormFactory $gameEventFormFactory)
    {
        $this->goalFormFactory = $goalFormFactory;
        $this->goalManager = $goalManager;
        $this->cardFormFactory = $cardFormFactory;
        $this->cardManager = $cardManager;
        $this->tourneyManager = $tourneyManager;
        $this->tourneyManagementManager = $tourneyManagementManager;
        $this->matchManager = $matchManager;
        $this->teamManager = $teamManager;
        $this->playerManager = $playerManager;
        $this->gameEventFormFactory = $gameEventFormFactory;
        $this->type = null;
        $this->timeFrom = null;
        $this->timeTo = null;
        $this->player = null;
    }

    public function playerAndTeamArrays($tourney, $match, $withAllOption = false){
        $playerArray = array();
        $teamArray = array();
        $firstTeam = $this->matchManager->get($match)[Model\MatchManager::COLUMN_FIRST_TEAM];
        $secondTeam = $this->matchManager->get($match)[Model\MatchManager::COLUMN_SECOND_TEAM];
        $nameOfFirstTeam = $this->teamManager->get($firstTeam)[Model\TeamManager::COLUMN_TITLE];
        $nameOfSecondTeam = $this->teamManager->get($secondTeam)[Model\TeamManager::COLUMN_TITLE];
        $playersInTheFirstTeam = $this->tourneyManagementManager->getPlayersByTeam($tourney, $firstTeam);
        $playersInTheSecondTeam = $this->tourneyManagementManager->getPlayersByTeam($tourney, $secondTeam);
        $teams = [$playersInTheFirstTeam, $playersInTheSecondTeam];
        if($withAllOption == true){
            $playerArray[-1] = "Všichni hráči";
        }
        foreach($teams as $team){
            foreach($team as $player){
                $playerID = $player[Model\TourneyManagementManager::SECOND_COLUMN_PLAYER];
                $playerEntity = $this->playerManager->get($playerID);
                $name = $playerEntity[Model\PlayerManager::COLUMN_NAME]." ".
                    $playerEntity[Model\PlayerManager::COLUMN_SURNAME];
                $playerArray[$playerID] = $name;
            }
        }
        $teamArray[$firstTeam] = $nameOfFirstTeam;
        $teamArray[$secondTeam] = $nameOfSecondTeam;
        return [$playerArray, $teamArray];
    }
    public function actionGameevents($tourney, $match, $pageGoal = 1, $pageCard = 1){
        $this->tourney = $tourney;
        $this->match = $match;
        $playerAndTeamArray = $this->playerAndTeamArrays($tourney, $match);
        $playerArray = $playerAndTeamArray[0];
        $teamArray = $playerAndTeamArray[1];
        $addGoalForm =  $this["addGoalForm"];
        $addGoalForm["player"]->setItems($playerArray);
        $addCardForm =  $this["addCardForm"];
        $playerAndTeamArray = $this->playerAndTeamArrays($tourney, $match, true);
        $playerArray = $playerAndTeamArray[0];
        $addCardForm["player"]->setItems($playerArray);
        $addCardForm["team"]->setItems($teamArray);
    }

    public function actionDeletegoal($tourney, $match, $goal){
        $this->goalManager->remove($goal);
        return $this->redirect(302, "Match:gameevents", ["tourney" => $tourney, "match" => $match]);
    }

    public function actionDeletecard($tourney, $match, $card){
        $this->cardManager->remove($card);
        return $this->redirect(302, "Match:gameevents", ["tourney" => $tourney, "match" => $match]);
    }

    public function actionEditgoal($tourney, $match, $goal){
        $this->tourney = $tourney;
        $this->match = $match;
        $this->editedGoal = $goal;
        $playerAndTeamArray = $this->playerAndTeamArrays($tourney, $match);
        $playerArray = $playerAndTeamArray[0];
        $editGoal = $this["editGoalForm"];
        $editGoal["player"]->setItems($playerArray);
        $goalEntity = $this->goalManager->get($goal);
        $time = $goalEntity[Model\GoalManager::COLUMN_TIME];
        $timeArray = explode(':', $time);
        $editGoal->setDefaults(["minute" => intval($timeArray[0]),
                                "second" => intval($timeArray[1]),
                                "player" => $goalEntity[Model\GoalManager::COLUMN_PLAYER]]);
    }

    public function actionEditcard($tourney, $match, $card){
        $this->tourney = $tourney;
        $this->match = $match;
        $this->editedCard = $card;
        $playerAndTeamArray = $this->playerAndTeamArrays($tourney, $match);
        $playerArray = $playerAndTeamArray[0];
        $editCard = $this["editCardForm"];
        $editCard["player"]->setItems($playerArray);
        $cardEntity = $this->cardManager->get($card);
        $time = $cardEntity[Model\CardManager::COLUMN_TIME];
        $timeArray = explode(':', $time);
        $editCard->setDefaults(["minute" => intval($timeArray[0]),
            "second" => intval($timeArray[1]),
            "player" => $cardEntity[Model\CardManager::COLUMN_PLAYER],
            "typeOfCard" => $cardEntity[Model\CardManager::COLUMN_TYPE]]);
    }

    public function createComponentFilterForm(){
        $lengthOfMatch = $this->tourneyManager->get($this->tourney)[Model\TourneyManager::COLUMN_MATCH_LENGTH];
        $form = $this->gameEventFormFactory->createFilterForm($this->tourney, $this->match, $lengthOfMatch);
        $form->onSuccess[] = function(Form $form, $values){
            $types = "";
            foreach($values["typeOfGameEvent"] as $type){
                $types.=$type;
            }
            $timeFrom = $values["minuteFrom"].":".$values["secondFrom"];
            $timeTo = $values["minuteTo"].":".$values["secondTo"];
            $this->type = $types;
            $this->timeFrom = $timeFrom;
            $this->timeTo = $timeTo;
            $this->player = $values["player"];
        };
        return $form;
    }

    public function createComponentAddGoalForm()
    {
        $lengthOfMatch = $this->tourneyManager->get($this->tourney)[Model\TourneyManager::COLUMN_MATCH_LENGTH];
        $form = $this->goalFormFactory->create($this->match, $lengthOfMatch);
        return $form;
    }

    public function createComponentAddCardForm()
    {
        $lengthOfMatch = $this->tourneyManager->get($this->tourney)[Model\TourneyManager::COLUMN_MATCH_LENGTH];
        $form = $this->cardFormFactory->create($this->match, $lengthOfMatch);
        return $form;
    }


    public function createComponentEditGoalForm(){
        $lengthOfMatch = $this->tourneyManager->get($this->tourney)[Model\TourneyManager::COLUMN_MATCH_LENGTH];
        return $this->goalFormFactory->create($this->match, $lengthOfMatch, true, intval($this->editedGoal));
    }

    public function createComponentEditCardForm(){
        $lengthOfMatch = $this->tourneyManager->get($this->tourney)[Model\TourneyManager::COLUMN_MATCH_LENGTH];
        return $this->cardFormFactory->create($this->match, $lengthOfMatch, true, intval($this->editedCard));
    }

    public function renderEditgoal($tourney, $match, $goal){
        $this->template->tourneyID = $tourney;
        $this->template->tourney = $this->tourneyManager->get($tourney);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $this->template->matchID = $match;
        $this->template->goalID = $goal;
    }
    public function renderEditcard($tourney, $match, $card){
        $this->template->tourneyID = $tourney;
        $this->template->tourney = $this->tourneyManager->get($tourney);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $this->template->matchID = $match;
        $this->template->cardID = $card;
    }
    public function renderGameevents($tourney, $match, $pageGoal = 1, $pageCard = 1){
        $this->template->tourney = $this->tourneyManager->get($tourney);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $this->template->matchID = $match;
        $this->template->tourneyID = $tourney;
        $goals = $this->goalManager->filterGoals($match, $this->type, $this->timeFrom, $this->timeTo, $this->player);
        $paginator = new Paginator();
        $paginator->setItemCount(count($goals));
        $paginator->setItemsPerPage(10);
        $paginator->setPage($pageGoal);
        $this->template->paginator = $paginator;
        $this->template->goals =  $this->goalManager->filterGoals($match, $this->type,
            $this->timeFrom, $this->timeTo, $this->player, $paginator->getLength(), $paginator->getOffset());
        $cards = $this->cardManager->filterCards($match, $this->type, $this->timeFrom, $this->timeTo, $this->player);
        $paginator2 = new Paginator();
        $paginator2->setItemCount(count($cards));
        $paginator2->setItemsPerPage(10);
        $paginator2->setPage($pageCard);
        $this->template->paginator2 = $paginator2;
        $this->template->cards = $this->cardManager->filterCards($match, $this->type,
            $this->timeFrom, $this->timeTo, $this->player, $paginator2->getLength(), $paginator2->getOffset());
        $this->template->goalID = Model\GoalManager::COLUMN_ID;
        $this->template->cardID = Model\CardManager::COLUMN_ID;
        $this->template->timeGoal = Model\GoalManager::COLUMN_TIME;
        $this->template->timeCard = Model\CardManager::COLUMN_TIME;
        $this->template->playerGoal = Model\GoalManager::COLUMN_PLAYER;
        $this->template->playerCard = Model\CardManager::COLUMN_PLAYER;
        $this->template->kindOfCard = Model\CardManager::COLUMN_TYPE;
        $matchEntity = $this->matchManager->get($match);
        $firstTeamID = $matchEntity[Model\MatchManager::COLUMN_FIRST_TEAM];
        $secondTeamID = $matchEntity[Model\MatchManager::COLUMN_SECOND_TEAM];
        $firstTeam = $this->teamManager->get($firstTeamID);
        $secondTeam = $this->teamManager->get($secondTeamID);
        $this->template->firstTeam = $firstTeam[Model\TeamManager::COLUMN_ABBREVIATION];
        $this->template->secondTeam = $secondTeam[Model\TeamManager::COLUMN_ABBREVIATION];
        $this->template->firstTeamGoals = $this->goalManager->getGoalsByTeam($tourney, $match, $firstTeamID);
        $this->template->secondTeamGoals = $this->goalManager->getGoalsByTeam($tourney, $match, $secondTeamID);
        $this->template->addFilter("nameOfPlayer", function($player){
            $playerEntity = $this->playerManager->get($player);
            if(empty($playerEntity)){
                return "Hráč s tímto ID neexistuje.";
            }
            return $playerEntity[Model\PlayerManager::COLUMN_NAME]." ".$playerEntity[Model\PlayerManager::COLUMN_SURNAME];
        });
        $this->template->addFilter("abbreviationOfTeam", function($team){
            $teamEntity = $this->teamManager->get($team);
            if(empty($teamEntity)){
                return "Tým s tímto ID neexistuje.";
            }
            return $teamEntity[Model\TeamManager::COLUMN_ABBREVIATION];
        });
        $this->template->addFilter("teamOfPlayer", function($player){
            $playerEntity = $this->playerManager->get($player);
            if(empty($playerEntity)){
                return "Hráč s tímto ID neexistuje.";
            }
            $teamOfPlayer = null;
            $teamsOfPlayer = $this->tourneyManagementManager->getTeamByPlayerAndTourney($this->tourney, $player);
            foreach($teamsOfPlayer as $team){
                $teamOfPlayer = $team[Model\TourneyManagementManager::COLUMN_TEAM];
            }
            return $teamOfPlayer;
        });
        $this->template->addFilter("titleOfCard", function($card){
            switch($card) {
                case 'r':
                    return "Červená karta - vyloučení";
                case 'y':
                    return "Žlutá karta - výstraha";
                case 'g':
                    return "Zelená karta - varování";
                default:
                    return "Takový druh trestu neexistuje.";
            }
        });
    }

}
