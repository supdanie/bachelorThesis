<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Utils\DateTime;
use Nette\Utils\Paginator;


class PlayerPresenter extends BasePresenter
{
    /** @var Forms\PlayerFormFactory */
    private $playerFactory;
    private $playerManager;
    private $editedPlayer;
    /** @persistent */
    public $name;
    /** @persistent */
    public $surname;
    /** @persistent */
    public $birthdateFrom;
    /** @persistent */
    public $birthdateTo;
    /** @persistent */
    public $sex;
    /** @persistent */
    public $number;


    /**
     * PlayerPresenter constructor.
     * @param Forms\PlayerFormFactory $playerFormFactory
     */
    public function __construct(Forms\PlayerFormFactory $playerFormFactory,
        Model\PlayerManager $playerManager)
    {
        $this->playerFactory = $playerFormFactory;
        $this->playerManager = $playerManager;
        $this->name = null;
        $this->surname = null;
        $this->birthdateFrom = null;
        $this->birthdateTo = null;
        $this->sex = null;
        $this->number = null;
    }

    /**
     * @return Form
     */
    public function createComponentAddForm()
    {
        return $this->playerFactory->create(false);
    }

    public function createComponentFilterForm(){
        $form = $this->playerFactory->createFilterForm();
        $form->onSuccess[] = [$this, "filterPlayers"];
        return $form;
    }

    public function filterPlayers(Form $form, $values){
        $sex = 0;
        if(in_array("male", $values["sex"])){
            $sex+=4;
        }
        if(in_array("female", $values["sex"])){
            $sex+=2;
        }
        if(in_array("unknown", $values["sex"])){
            $sex+=1;
        }
        $this->sex = $sex;
        $this->name = $values["name"];
        $this->surname = $values["surname"];
        $this->birthdateFrom = empty($values["birthdateFrom"]) ? null : $values["birthdateFrom"]->format("d.m.Y");
        $this->birthdateTo = empty($values["birthdateTo"]) ? null : $values["birthdateTo"]->format("d.m.Y");
        $this->number = $values["number"];
    }

    public function actionEdit($id){
        $this->editedPlayer = $id;
        $form = $this["editForm"];
        $form->onSuccess[] = function (Form $form, $values){
            $this->playerFactory->addOrEditPlayer($form, $values, true, $values["id"]);
            $this->redirect("default");
        };
    }
    /**
     * @param $id
     */
    public function actionDelete($id){
        $this->playerManager->remove($id);
        return $this->redirect(303, "Player:default");
    }
    /**
     * @return Form
     */
    public function createComponentEditForm()
    {
        $id = $this->editedPlayer;
        $player = $this->playerManager->get($id);
        $values = $player->toArray();
        $name = $values[Model\PlayerManager::COLUMN_NAME];
        $surname = $values[Model\PlayerManager::COLUMN_SURNAME];
        $birthdate = $values[Model\PlayerManager::COLUMN_BIRTHDATE];
        $sexLetter = $values[Model\PlayerManager::COLUMN_SEX];
        $sex = "unknown";
        if($sexLetter == "m"){
            $sex = "male";
        } else if($sexLetter == "f"){
            $sex = "female";
        }
        $number = $values[Model\PlayerManager::COLUMN_NUMBER];
        $form =  $this->playerFactory->create(true, $id)->
        setDefaults(["name" => $name,
                    "surname" => $surname,
                    "birthdate" => $birthdate,
                    "sex" => $sex,
                    "number" => $number]);
        return $form;
    }


    public function createSexArrayFromNumber($sexNumber){
        $sex=array();
        if($sexNumber%8 >= 4){
            array_push($sex, "m");
        }
        if($sexNumber%4 >= 2){
            array_push($sex, "f");
        }
        if($sexNumber%2 == 1){
            array_push($sex, "u");
        }
        return $sex;
    }
    /**
     *
     */
    public function renderDefault($page = 1){
        $startBirthdate = $this->birthdateFrom === null ? null : new DateTime($this->birthdateFrom);
        $endBirthdate = $this->birthdateTo === null ? null : new DateTime($this->birthdateTo);
        $sexOfPlayers = $this->sex === null ? null : $this->createSexArrayFromNumber($this->sex);
        $players = $this->playerManager->filterPlayers($this->name, $this->surname,
            $startBirthdate, $endBirthdate, $sexOfPlayers, $this->number);
        $paginator = new Paginator();
        $paginator->setItemCount(count($players));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->players = $this->playerManager->filterPlayers($this->name, $this->surname,
            $startBirthdate, $endBirthdate, $sexOfPlayers, $this->number, $paginator->getLength(), $paginator->getOffset());
        $this->template->id = Model\PlayerManager::COLUMN_ID;
        $this->template->name = Model\PlayerManager::COLUMN_NAME;
        $this->template->surname = Model\PlayerManager::COLUMN_SURNAME;
        $this->template->birthdate = Model\PlayerManager::COLUMN_BIRTHDATE;
        $this->template->sex = Model\PlayerManager::COLUMN_SEX;
    }

    public function renderEdit($id){
        $this->template->player = $this->playerManager->get($id);
        $this->template->id = Model\PlayerManager::COLUMN_ID;
    }


}
