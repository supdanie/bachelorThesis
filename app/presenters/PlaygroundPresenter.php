<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Paginator;

class PlaygroundPresenter extends BasePresenter
{
    private $playgroundFactory;
    private $playgroundManager;
    private $editedPlayground;
    /** @persistent */
    public $colour;
    /** @persistent */
    public $from;
    /** @persistent */
    public $to;
    public function __construct(Forms\PlaygroundFormFactory $playgroundFormFactory,
    Model\PlaygroundManager $playgroundManager)
    {
        $this->playgroundFactory = $playgroundFormFactory;
        $this->playgroundManager = $playgroundManager;
        $this->colour = null;
        $this->from = null;
        $this->to = null;
    }

    /**
     * @param $id
     */
    public function actionDelete($id){
        $this->playgroundManager->remove($id);
        return $this->redirect(303, "Playground:default");
    }

    public function actionEdit($id){
        $this->editedPlayground = $id;
        $form = $this["editForm"];
        $form->onSuccess[] = function (Form $form, $values){
            $this->playgroundFactory->addOrEditPlayground($values, true, $values["id"]);
            $this->redirect("default");
        };
    }

    public function createComponentAddForm(){
        $form = $this->playgroundFactory->create(false);
        $form->setDefaults(array(
            'position' => array(
                'latitude' => "49.1695254488",
                'longitude' => "14.2521617334",
            ),
        ));
        return $form;
    }

    public function createComponentFilterForm(){
        $form = $this->playgroundFactory->createFilterForm();
        $form->onSuccess[] = function (Form $form, $values){
            $this->colour = $values["color"];
            $this->from = empty($values["from"]) ? null : $values["from"]->format("d.m.Y");
            $this->to = empty($values["to"]) ? null : $values["to"]->format("d.m.Y");
        };
        return $form;
    }

    public function createComponentEditForm(){
        $id = $this->editedPlayground;
        $playground = $this->playgroundManager->get($this->editedPlayground);
        $form = $this->playgroundFactory->create(true, $id);
        $values = $playground->toArray();
        $colour = $values[Model\PlaygroundManager::COLUMN_COLOUR];
        $GPSString = $values[Model\PlaygroundManager::COLUMN_GPS];
        $GPSarray = explode(',', $GPSString);
        $dateOfFoundation = $values[Model\PlaygroundManager::COLUMN_DATE_OF_FOUNDATION];
        $description = $values[Model\PlaygroundManager::COLUMN_DESCRIPTION];
        $form->setDefaults(["color" => $colour, "position" => array("latitude" => doubleval($GPSarray[0]),
        "longitude" => doubleval($GPSarray[1])),

            "dateOfFoundation" => $dateOfFoundation, "description" => $description]);
        return $form;
    }

    public function renderDefault($page = 1){
        $dateFrom = $this->from == null ? null :new DateTime($this->from);
        $dateTo = $this->to == null ? null : new DateTime($this->to);
        $playgrounds = $this->playgroundManager->filterPlaygrounds($this->colour, $dateFrom, $dateTo);
        $paginator = new Paginator();
        $paginator->setItemCount(count($playgrounds));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->playgrounds = $this->playgroundManager->filterPlaygrounds($this->colour, $dateFrom, $dateTo,
                $paginator->getLength(), $paginator->getOffset());
        $this->template->id = Model\PlaygroundManager::COLUMN_ID;
    }

    public function renderEdit($id){
        $this->template->playground = $this->playgroundManager->get($id);
        $this->template->id = Model\PlaygroundManager::COLUMN_ID;
    }
}
