<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Paginator;

class PlayingdaysPresenter extends BasePresenter
{
    private $playingDayFactory;
    private $playingDayManager;
    private $playground;
    private $editedPlayingDay;
    /** @persistent */
    public $from;
    /** @persistent */
    public $to;
    public function __construct(Forms\PlayingDayFormFactory $playingDayFormFactory,
    Model\PlayingDayManager $playingDayManager)
    {
        $this->playingDayFactory = $playingDayFormFactory;
        $this->playingDayManager = $playingDayManager;
        $this->from = null;
        $this->to = null;
    }

    public function actionDefault($id, $page = 1){
        $this->playground = $id;
    }

    /**
     * @param $id
     */
    public function actionDelete($id,$id2){
        $this->playingDayManager->remove($id2);
        return $this->redirect(303, "Playingdays:default", ["id" => $id]);
    }


    public function actionEdit($id, $id2){
        $this->playground = $id;
        $this->editedPlayingDay = $id2;
        $form = $this["editForm"];
        $form->onSuccess[] = function (Form $form, $values){
            $this->playingDayFactory->addOrEditPlayingDay($values, true, $values["playground"], $values["id"]);
            $this->redirect(303, "Playingdays:default", ["id" => $this->playground]);
        };
    }

    public function getNumberFromString($string){
        return substr($string, 0,1) == "0" ? intval(substr($string, 1)) : intval($string);
    }

    public function createComponentFilterForm(){
        $form = $this->playingDayFactory->createFilterForm();
        $form->onSuccess[] = function (Form $form, $values){
            $this->from = empty($values["from"]) ? null : $values["from"]->format("d.m.Y");
            $this->to = empty($values["to"]) ? null : $values["to"]->format("d.m.Y");
        };
        return $form;
    }
    public function createComponentEditForm(){
        $id = $this->playground;
        $playingDayID = $this->editedPlayingDay;;
        $playingDay = $this->playingDayManager->get($playingDayID);
        $form = $this->playingDayFactory->create(true, $id, $playingDayID);
        $day = $playingDay[Model\PlayingDayManager::COLUMN_DAY];
        $from = $playingDay[Model\PlayingDayManager::COLUMN_FROM];
        $to = $playingDay[Model\PlayingDayManager::COLUMN_TO];
        $form->setDefaults(["day" => $day,
            "fromHour" => $this->getNumberFromString(explode(':', $from)[0]),
            "fromMinute" => $this->getNumberFromString(explode(':', $from)[1]),
            "toHour" => $this->getNumberFromString(explode(':', $to)[0]),
            "toMinute" => $this->getNumberFromString(explode(':', $to)[1])]);
        return $form;
    }

    public function createComponentAddForm(){
        $form = $this->playingDayFactory->create(false, $this->playground);
        return $form;
    }

    /**
     * @param $id
     */
    public function renderDefault ($id, $page = 1)
    {
        $dateFrom = $this->from == null ? null :new DateTime($this->from);
        $dateTo = $this->to == null ? null :new DateTime($this->to);
        $playingDays = $this->playingDayManager->filterPlayingDays($id,$dateFrom, $dateTo);
        $paginator = new Paginator();
        $paginator->setItemCount(count($playingDays));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->playingDays = $this->playingDayManager->filterPlayingDays($id,$dateFrom, $dateTo, false,
                $paginator->getLength(), $paginator->getOffset());
        $this->template->id = Model\PlayingDayManager::COLUMN_ID;
        $this->template->day = Model\PlayingDayManager::COLUMN_DAY;
        $this->template->from = Model\PlayingDayManager::COLUMN_FROM;
        $this->template->to = Model\PlayingDayManager::COLUMN_TO;
        $this->template->playground = $id;
    }

    public function renderEdit($id, $id2){
        $this->template->playgroundID = $id;
        $this->template->playingDayID = $id2;
    }
}
