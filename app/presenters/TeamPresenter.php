<?php

namespace App\Presenters;

use App\Forms;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Utils\Paginator;


class TeamPresenter extends BasePresenter
{
    /** @var Forms\TeamFormFactory */
    private $teamFactory;
    private $categoryManager;
    private $teamManager;
    private $editedCategory;
    /** @persistent */
    public $title;
    /** @persistent */
    public $abbreviation;
    /** @persistent */
    public $category;
    /** @persistent */
    public $primaryColour;
    /** @persistent */
    public $secondaryColour;
    /**
     * TeamPresenter constructor.
     * @param Forms\TeamFormFactory $teamFormFactory
     */
    public function __construct(Forms\TeamFormFactory $teamFormFactory,
                                Model\CategoryManager $categoryManager,
                                Model\TeamManager $teamManager)
    {
        $this->teamFactory = $teamFormFactory;
        $this->categoryManager = $categoryManager;
        $this->teamManager = $teamManager;
        $this->title = null;
        $this->abbreviation = null;
        $this->category = -1;
        $this->primaryColour = null;
        $this->secondaryColour = null;
    }


    public function actionEdit($id){
        $this->editedCategory = $id;
        $formEdit = $this["editForm"];
        $formEdit->onSuccess[] = function (Form $form, $values){
            $this->teamFactory->addOrEditTeam($form, $values, true, $values["id"]);
            $this->redirect("default");
        };
    }
    /**
     * @param $id
     */
    public function actionDelete($id){
        $this->teamFactory->getTeamManager()->remove($id);
        return $this->redirect(303, "Team:default");
    }

    /**
     *
     */
    public function renderDefault($page = 1)
    {
        $teams = $this->teamManager->filterTeamsByParameters($this->title,
            $this->abbreviation, $this->category, $this->primaryColour, $this->secondaryColour);
        $paginator = new Paginator();
        $paginator->setItemCount(count($teams));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->teams = $this->teamManager->filterTeamsByParameters($this->title,
            $this->abbreviation, $this->category, $this->primaryColour, $this->secondaryColour,
            $paginator->getLength(), $paginator->getOffset());
        $this->template->categories = $this->categoryManager->getAll();
        $this->template->categoryID = Model\TeamManager::COLUMN_CATEGORY;
        $this->template->titleOfCategory = Model\CategoryManager::COLUMN_TITLE;
        $this->template->id = Model\TeamManager::COLUMN_ID;
        $this->template->title = Model\TeamManager::COLUMN_TITLE;
        $this->template->abbreviation = Model\TeamManager::COLUMN_ABBREVIATION;
        $this->template->category = Model\TeamManager::COLUMN_CATEGORY;
        $this->template->primaryColour = Model\TeamManager::COLUMN_PRIMARY_COLOR;
        $this->template->secondaryColour = Model\TeamManager::COLUMN_SECONDARY_COLOR;
    }

    public function renderEdit($id){
        $this->template->team = $this->teamManager->get($id);
        $this->template->id = Model\TeamManager::COLUMN_ID;
    }
    public function createComponentFilterForm(){
        $form = $this->teamFactory->createFilterForm();
        $form->onSuccess[] = function (Form $form, $values){
            $this->title = $values["title"];
            $this->abbreviation = $values["abbreviation"];
            $this->category = $values["category"];
            $this->primaryColour = $values["primaryColour"];
            $this->secondaryColour = $values["secondaryColour"];
        };
        return $form;
    }
    /**
     * @return Form
     */
    public function createComponentAddForm()
    {
        $form = $this->teamFactory->create(false);
        return $form;
    }
    public function createComponentEditForm(){
        $id = $this->editedCategory;
        $team = $this->teamManager->get(intval($id));
        $values = $team->toArray();
        $title = $values[Model\TeamManager::COLUMN_TITLE];
        $abbreviation = $values[Model\TeamManager::COLUMN_ABBREVIATION];
        $categoryOfTeam = $values[Model\TeamManager::COLUMN_CATEGORY];
        $primaryColour = $values[Model\TeamManager::COLUMN_PRIMARY_COLOR];
        $secondaryColour = $values[Model\TeamManager::COLUMN_SECONDARY_COLOR];
        $form = $this->teamFactory->create(true,$id);
        $form->setDefaults(
            ["title" => $title,
                "abbreviation" => $abbreviation,
                "category" => $categoryOfTeam,
                "primaryColour" => $primaryColour,
                "secondaryColour" => $secondaryColour]
        );
        return $form;
    }
}
