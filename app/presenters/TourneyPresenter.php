<?php

namespace App\Presenters;

use App\Forms;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Utils\DateTime;
use Nette\Utils\Paginator;


class TourneyPresenter extends BasePresenter
{
    private $tourneyFactory;
    private $tourneyManager;
    private $teamManager;
    private $playerManager;
    private $refereeManager;
    private $categoryManager;
    private $tourneyManagementManager;
    private $playgroudManager;
    private $tourneyExporter;
    private $matchManager;
    private $managedTourney;
    private $managedTeam;
    /** @persistent */
    public $title;
    /** @persistent */
    public $city;
    /** @persistent */
    public $from;
    /** @persistent */
    public $to;
    /** @persistent */
    public $playground = null;
    /** @persistent */
    public $dateFrom = null;
    /** @persistent */
    public $timeFrom = null;
    /** @persistent */
    public $dateTo = null;
    /** @persistent */
    public $timeTo = null;
    /** @persistent */
    public $playingTeam = null;
    /** @persistent */
    public $referee = null;
    public function __construct(Forms\TourneyFormFactory $tourneyFormFactory,
    Model\TourneyManager $tourneyManager, Model\RefereeManager $refereeManager,
                                Model\TeamManager $teamManager, Model\PlayerManager $playerManager,
        Model\TourneyManagementManager $tourneyManagementManager,
        Model\CategoryManager $categoryManager,
        Model\PlaygroundManager $playgroundManager,
        Model\ExportAndImport\TourneyExporter $tourneyExporter,
        Model\MatchManager $matchManager)
    {
        $this->tourneyFactory = $tourneyFormFactory;
        $this->tourneyManager = $tourneyManager;
        $this->refereeManager = $refereeManager;
        $this->teamManager = $teamManager;
        $this->playerManager = $playerManager;
        $this->tourneyManagementManager = $tourneyManagementManager;
        $this->categoryManager = $categoryManager;
        $this->playgroudManager = $playgroundManager;
        $this->tourneyExporter = $tourneyExporter;
        $this->matchManager = $matchManager;
        $this->title = null;
        $this->city = null;
        $this->from = null;
        $this->to = null;
    }


    /**
     * @param $id
     */
    public function actionDelete($id){
        $this->tourneyManagementManager->removePlayersFromTourney($id);
        $this->tourneyManagementManager->removeTeamsFromTourney($id);
        $this->refereeManager->removeRefereeRulesByTourney($id);
        $this->tourneyManager->remove($id);
        return $this->redirect(303, "Tourney:default");
    }

    public function actionEdit($id){
        $this->managedTourney = $id;
        $form = $this["editForm"];
        $form->onSuccess[] =  function(Form $form, $values){
            $this->tourneyFactory->addOrEditTourney($values, true, $values["id"]);
            $this->redirect("default");
        };
    }

    public function actionMatchschedule($id, $page = 1){
        $this->managedTourney = $id;
        $form = $this["filterMatchesForm"];
        $playgrounds = $this->playgroudManager->playgroundsByTourney($id);
        $playgroundsForForm = ["any" => "Libovolné hřiště"];
        foreach($playgrounds as $playground){
            $id = $playground[Model\PlaygroundManager::COLUMN_ID];
            $playgroundsForForm[$id] = $id;
        }
        $teams = $this->tourneyManagementManager->getTeamsByTourney($id);
        $teamsForForm = ["any" => "Libovolný tým"];
        foreach($teams as $team){
            $teamID = $team[Model\TourneyManagementManager::COLUMN_TEAM];
            $teamEntity = $this->teamManager->get($teamID);
            $teamsForForm[$teamID] = $teamEntity[Model\TeamManager::COLUMN_ABBREVIATION];
        }
        $form["playground"]->setItems($playgroundsForForm);
        $form["playingTeam"]->setItems($teamsForForm);
        $form["referee"]->setItems($teamsForForm);
        $form->onSuccess[] = [$this,"filterMatches"];
    }

    public function filterMatches(Form $form, $values){
        $dateFrom = empty($values["dateFrom"]) ? null : $values["dateFrom"]->format("d.m.Y");
        $timeFrom = empty($dateFrom) ? null : $values["fromHour"].":".$values["fromMinute"];
        $dateTo = empty($values["dateTo"]) ? null : $values["dateTo"]->format("d.m.Y");
        $timeTo = empty($dateTo) ? null : $values["toHour"].":".$values["toMinute"];
        $this->playground = $values["playground"];
        $this->dateFrom = $dateFrom;
        $this->timeFrom = $timeFrom;
        $this->dateTo = $dateTo;
        $this->timeTo = $timeTo;
        $this->playingTeam = $values["playingTeam"];
        $this->referee = $values["referee"];
    }
    /**
     * @param $id
     * @param $category
     */
    public function actionDeletecategory($id, $category){
        $teams = $this->teamManager->getTeamsInCategory($category);
        foreach($teams as $team){
            $this->tourneyManagementManager->removeTeamFromTourney($id, $team[Model\TeamManager::COLUMN_ID]);
        }
        return $this->redirect(303, "Tourney:management", ["id" => $id]);
    }
    public function actionDeleteteam($id, $team){
        $this->tourneyManagementManager->removeTeamFromTourney($id, $team);
        return $this->redirect(303, "Tourney:management", ["id" => $id]);
    }
    public function actionDeleteplayer($id, $player){
        $this->tourneyManagementManager->removePlayerFromTourney($id, $player);
        return $this->redirect(303, "Tourney:management", ["id" => $id]);
    }

    public function actionManageteam($id, $team){
        $this->managedTourney = $id;
        $this->managedTeam = $team;
    }

    public function actionGeneratematchschedule($id){
        $this->managedTourney = $id;
    }


    public function actionSetcustomrules($id){
        $this->managedTourney = $id;
    }

    public function actionManagement($id){
        $this->managedTourney = intval($id);
        $playgroundsForm = $this["playgroundsForm"];
        $playgrounds = $this->playgroudManager->getAll();
        $playgroundsWithTourney = array();
        foreach($playgrounds as $playground){
            if($playground[Model\PlaygroundManager::COLUMN_TOURNEY] == $id){
                array_push($playgroundsWithTourney, $playground[Model\PlaygroundManager::COLUMN_ID]);
            }
        }
        $playgroundsForm->setDefaults(["playgrounds" => $playgroundsWithTourney]);
        $teams = $this->tourneyManagementManager->getTeamsByTourney($id);
        foreach($teams as $team){
            $changeSeedForm = $this["changeSeedForm-".$team[Model\TourneyManagementManager::COLUMN_TEAM]];
            $seed = $team[Model\TourneyManagementManager::COLUMN_SEED];
            $changeSeedForm->setDefaults(["seed" => $seed]);
        }
    }

    public function createComponentAddForm(){
        $form = $this->tourneyFactory->create(false);
        $form->onSuccess[] = function(Form $form, $values){
            if($values["rules"] == "customRules"){
                $maxID = $this->tourneyManager->getMaximumID();
                $this->redirect(303, "Tourney:setcustomrules", ["id" => $maxID]);
            }
        };
        return $form;
    }

    public function createComponentAddTeamForm(){
        $id = $this->managedTourney;
        return $this->tourneyFactory->createAddTeamForm($id);
    }

    public function createComponentAddPlayerToTheTeamForm(){
        $id = $this->managedTourney;
        $team = $this->managedTeam;
        return $this->tourneyFactory->createAddPlayerToTeamForm($id, $team);
    }

    public function createComponentChangeCaptainForm(){
        $tourney = $this->managedTourney;
        $team = $this->managedTeam;
        return $this->tourneyFactory->createChangeCaptainForm($tourney, $team);
    }
    public function createComponentPlaygroundsForm(){
        $id = $this->managedTourney;
        $playgrounds = $this->playgroudManager->getAll();
        $suitablePlaygrounds = array();
        foreach($playgrounds as $playground){
            if($playground[Model\PlaygroundManager::COLUMN_TOURNEY] === null ||
                $playground[Model\PlaygroundManager::COLUMN_TOURNEY] == $id){
                array_push($suitablePlaygrounds, $playground);
            }
        }
        return $this->tourneyFactory->createPlaygroundsForm($suitablePlaygrounds, $id);
    }

    public function createComponentChangeSeedForm(){
        return new Multiplier(function($team){
            $id = $this->managedTourney;
            $form = $this->tourneyFactory->createChangeSeedForm($id, $team);
            return $form;
        });
    }

    public function createComponentChangePlayerForm(){
        return new Multiplier(function($player){
            $id = intval($this->managedTourney);
            $form = $this->tourneyFactory->createChangePlayerForm($id, intval($player));
            return $form;
        });
    }

    public function createComponentGenerateMatchScheduleForm(){
        $id = $this->managedTourney;
        $form = $this->tourneyFactory->createGenerateMatchScheduleForm($id);
        $form->onSuccess[] = function(Form $form, $values){
            $this->redirect(303, "Tourney:default");
        };
        return $form;
    }

    public function createComponentSetCustomRulesForm(){
        $id = intval($this->managedTourney);
        return $this->tourneyFactory->createSetCustomRulesForm($id);
    }


    public function createComponentFilterForm(){
        $form = $this->tourneyFactory->createFilterForm();
        $form->onSuccess[] = function(Form $form, $values){
            $this->title = $values["title"];
            $this->city = $values["city"];
            $this->from = empty($values["from"]) ? null : $values["from"]->format("d.m.Y");
            $this->to = empty($values["to"]) ? null : $values["to"]->format("d.m.Y");
        };
        return $form;
    }

    public function createComponentFilterMatchesForm(){
        $form = $this->tourneyFactory->createFilterMatchesForm();
        return $form;
    }

    public function createComponentEditForm()
    {
        $id = $this->managedTourney;
        $tourney = $this->tourneyManager->get(intval($id));
        $form = $this->tourneyFactory->create(true, intval($id));
        $values = $tourney->toArray();
        $title = $values[Model\TourneyManager::COLUMN_TITLE];
        $city = $values[Model\TourneyManager::COLUMN_CITY];
        $from = $values[Model\TourneyManager::COLUMN_FROM];
        $to = $values[Model\TourneyManager::COLUMN_TO];
        $description = $values[Model\TourneyManager::COLUMN_DESCRIPTION];
        $program = $values[Model\TourneyManager::COLUMN_PROGRAM];
        $matchLength = intval($values[Model\TourneyManager::COLUMN_MATCH_LENGTH]);
        $referringRules = $this->refereeManager->canRefer(intval($id));
        $form->setDefaults(["title" => $title,
        "city" => $city,
        "from" => $from,
        "to" => $to,
            "rules" => $referringRules,
            "matchLength" => $matchLength,
        "description" => $description,
        "program" => $program]);
        return $form;
    }

    public function renderDefault($page = 1)
    {
        $dateFrom = empty($from) ? null : new DateTime($from);
        $dateTo = empty($to) ? null : new DateTime($to);
        $tourneys = $this->tourneyManager->filterTourneys($this->title, $this->city,
            $this->dateFrom, $this->dateTo);
        $this->template->tourneys = $tourneys;
        $paginator = new Paginator();
        $paginator->setItemCount(count($tourneys));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->paginator = $paginator;
        $this->template->pagedTourneys = $this->tourneyManager->
        filterTourneys($this->title, $this->city, $this->dateFrom, $this->dateTo,
            $paginator->getLength(), $paginator->getOffset());
        $this->template->matchManager = $this->matchManager;
        $this->template->refereeManager = $this->refereeManager;
        $this->template->id = Model\TourneyManager::COLUMN_ID;
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $this->template->city = Model\TourneyManager::COLUMN_CITY;
        $this->template->from = Model\TourneyManager::COLUMN_FROM;
        $this->template->to = Model\TourneyManager::COLUMN_TO;
    }


    public function renderManagement($id){
        $this->template->id = $id;
        $this->template->tourney = $this->tourneyManager->get($id);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $teams = $this->tourneyManagementManager->getTeamsByTourney($id);
        $categories = array();
        foreach($teams as $teamEntity){
            $teamID = $teamEntity[Model\TourneyManagementManager::COLUMN_TEAM];
            $team = $this->teamManager->get($teamID);
            if(!isset($categories[$team[Model\TeamManager::COLUMN_CATEGORY]])){
                $categories[$team[Model\TeamManager::COLUMN_CATEGORY]] = array();
            }
            $teamWithoutPlayers = array($team[Model\TeamManager::COLUMN_TITLE], $teamID);
            array_push($categories[$team[Model\TeamManager::COLUMN_CATEGORY]],$teamWithoutPlayers);
        }
        $this->template->categoriesTeamsAndPlayers = $categories;
        $this->template->categories = $this->categoryManager->getAll();
        $this->template->players = $this->playerManager->getAll();
        $this->template->titleOfCategory = Model\CategoryManager::COLUMN_TITLE;
        $this->template->titleOfTeam = Model\TeamManager::COLUMN_TITLE;
        $this->template->abbreviationOfTeam = Model\TeamManager::COLUMN_ABBREVIATION;
        $this->template->suitableTeams = $this->tourneyFactory->getTeams($id, false);

    }


    public function renderManageteam($id, $team){
        $this->template->id = Model\TourneyManager::COLUMN_ID;
        $this->template->tourney = $this->tourneyManager->get($id);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $this->template->idOfTeam = $team;
        $players = $this->tourneyManagementManager->getPlayersByTeam($id, $team);
        $teamEntities = $this->tourneyManagementManager->getTeamByTourneyAndId($id, $team);
        $teamEntity = null;
        foreach($teamEntities as $teamEnt){
            $teamEntity = $teamEnt;
        }
        $teamInfo = $this->teamManager->get($team);
        $teamOfPlayers = array();
        foreach($players as $playerEntity) {
            $player = $this->playerManager->get($playerEntity[Model\TourneyManagementManager::SECOND_COLUMN_PLAYER]);
            array_push($teamOfPlayers , $player);
        }
        $teamWithPlayers = array($teamInfo[Model\TeamManager::COLUMN_TITLE], $teamOfPlayers,
            $teamInfo[Model\TeamManager::COLUMN_ID], $teamEntity[Model\TourneyManagementManager::COLUMN_CAPTAIN]);
        $this->template->team = $teamWithPlayers;
        $this->template->playerID = Model\PlayerManager::COLUMN_ID;
        $this->template->nameOfPlayer = Model\PlayerManager::COLUMN_NAME;
        $this->template->surnameOfPlayer = Model\PlayerManager::COLUMN_SURNAME;
        $this->template->suitablePlayers = $this->tourneyFactory->getPlayers($id, $team);
    }

    public function renderGeneratematchschedule($id){
        $this->template->categories = $this->tourneyFactory->getCategoriesForForm($id);
        $this->template->tourney = $this->tourneyManager->get($id);
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
    }
    public function renderMatchschedule($id, $page = 1){
        $fromDate = $this->dateFrom === null ? null : new DateTime($this->dateFrom);
        $toDate = $this->dateTo === null ? null : new DateTime($this->dateTo);
        $this->template->tourney = $this->tourneyManager->get($id);
        $this->template->tourneyID = $id;
        $this->template->title = Model\TourneyManager::COLUMN_TITLE;
        $matches = $this->matchManager->filterMatches($id, $this->playground, $fromDate,
            $this->timeFrom, $toDate, $this->timeTo, $this->playingTeam, $this->referee);
        $paginator = new Paginator();
        $paginator->setItemCount(count($matches));
        $paginator->setItemsPerPage(20);
        $paginator->setPage($page);
        $this->template->matches = $this->matchManager->filterMatches($id, $this->playground, $fromDate,
            $this->timeFrom, $toDate, $this->timeTo, $this->playingTeam, $this->referee,
            $paginator->getLength(), $paginator->getOffset());
        $this->template->paginator = $paginator;
        $this->template->matchID = Model\MatchManager::COLUMN_ID;
        $this->template->timeFrom = Model\MatchManager::COLUMN_TIME;
        $this->template->firstTeam = Model\MatchManager::COLUMN_FIRST_TEAM;
        $this->template->secondTeam = Model\MatchManager::COLUMN_SECOND_TEAM;
        $this->template->referee = Model\MatchManager::COLUMN_REFEREE;
        $this->template->playground = "playground";
        $this->template->date = "date";
    }

    public function renderEdit($id){
        $this->template->id = Model\TourneyManager::COLUMN_ID;
        $this->template->tourney = $this->tourneyManager->get($id);
    }

    public function renderSetcustomrules($id){
        $this->template->id = Model\TourneyManager::COLUMN_ID;
        $this->template->tourney = $this->tourneyManager->get($id);
        $this->template->categories = $this->categoryManager->getAll();
        $this->template->secondCategories = $this->categoryManager->getAll();
        $this->template->categoryID = Model\CategoryManager::COLUMN_ID;
        $this->template->titleOfCategory = Model\CategoryManager::COLUMN_TITLE;
    }

}
