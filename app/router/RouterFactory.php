<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = new Route('', 'Homepage:default');
		$router[] = new Route('sign/<action>', 'Sign:default');
        $router[] = new Route('category/<action>/[<id>]', 'Category:default');
        $router[] = new Route('team/<action>/[<id>]', 'Team:default');
        $router[] = new Route('player/<action>/[<id>]', 'Player:default');
        $router[] = new Route('playground/<action>/[<id>]', 'Playground:default');
        $router[] = new Route('playground/playing_days/<id>/<action>/[<id2>]', 'Playingdays:default');
        $router[] = new Route('tourney', 'Tourney:default');
        $router[] = new Route('tourney/edit/<id>', 'Tourney:edit');
        $router[] = new Route('tourney/delete/<id>', 'Tourney:delete');
        $router[] = new Route('tourney/setcustomrules/<id>', 'Tourney:setcustomrules');
        $router[] = new Route('tourney/management/<id>', 'Tourney:management');
        $router[] = new Route('tourney/management/<id>/manageteam/<team>', 'Tourney:manageteam');
        $router[] = new Route('tourney/management/<id>/deleteplayer/<player>', 'Tourney:deleteplayer');
        $router[] = new Route('tourney/management/<id>/deleteteam/<team>', 'Tourney:deleteteam');
        $router[] = new Route('tourney/management/<id>/deletecategory/<category>', 'Tourney:deletecategory');
        $router[] = new Route('tourney/generateMatchSchedule/<id>', "Tourney:generatematchschedule");
        $router[] = new Route('tourney/matchSchedule/<id>', "Tourney:matchschedule");
        $router[] = new Route('tourney/matchSchedule/<tourney>/gameevents/<match>', 'Match:gameevents');
        $router[] = new Route('tourney/matchSchedule/<tourney>/gameevents/<match>/removeGoal/<goal>', 'Match:deletegoal');
        $router[] = new Route('tourney/matchSchedule/<tourney>/gameevents/<match>/removeCard/<card>', 'Match:deletecard');
        $router[] = new Route('tourney/matchSchedule/<tourney>/gameevents/<match>/editGoal/<goal>', 'Match:editgoal');
        $router[] = new Route('tourney/matchSchedule/<tourney>/gameevents/<match>/editCard/<card>', 'Match:editcard');
        return $router;
	}
}
