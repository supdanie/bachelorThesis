#ifndef GROUP
#define GROUP
#include "Group.cpp"
#endif
#include <vector>

using namespace std;

struct Category{
    string title;
    int numberOfGroups;
    vector<Team> teams;
    vector<Group> groups;
    static string actualGroup;
    Category(string title = ""){
        this->title = title;
    }
    Category(string title, int numberOfGroups, vector<Team> teams){
        this->title = title;
        this->numberOfGroups = numberOfGroups;
        this->teams = teams;
        this->addTeamsToGroups();
    }
    
    void incrementGroup(){
        int length = actualGroup.size();
        int index = length - 1;
        while(index >= 0 && actualGroup[index] == 'Z'){
            actualGroup[index] = 'A';
            index--;
        }
        if(index < 0){
           actualGroup = "A"+actualGroup; 
        } else {
           actualGroup[index]++;
        }
    }
    
    void addTeamsToGroups(){
        for(int i = 0; i < numberOfGroups; i++){
            Group group(actualGroup);
            incrementGroup();
            groups.push_back(group);
        }
        unsigned int groupsCount = groups.size();
        for(unsigned int i = 0; i < teams.size(); i++){
            groups[i%groupsCount].teams.push_back(teams[i]);
        }
    }
    
    static Category convertJson(Json::Value title, Json::Value numberOfGroups, Json::Value teams){
        string titleString = title.asString();
        Json::Value::ArrayIndex i;
        vector<Team> teamsInCategory;
        int groupCount = numberOfGroups.asInt();
        for(i = 0; i < teams.size(); i++){
            Team team = Team::convertJson(teams[i]["id"],
                    teams[i]["abbreviation"], teams[i]["numberOfPlayers"]);
            teamsInCategory.push_back(team);
        }
        return Category(titleString, groupCount, teamsInCategory);
    }
};