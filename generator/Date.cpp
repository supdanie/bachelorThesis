#include <jsoncpp/json/json.h>

struct Date{
    int day;
    int month;
    int year;
    Date(int day = 1, int month = 1, int year = 1900){
        this->day = day;
        this->month = month;
        this->year = year;
    }
    bool isSame(Date date){
        return date.year == this->year && date.month == this->month && date.day == this->day;
    }
    
    
    bool isEarlierOrSame(Date date){
        if(date.year < this->year){
            return false;
        }
        if(date.year == this->year && date.month < this->month){
            return false;
        }
        if(date.year == this->year && date.month == this->month && date.day < this->day){
            return false;
        }
        return true;
    }
    static Date convertJson(Json::Value day, Json::Value month, Json::Value year){
        return Date(day.asInt(), month.asInt(), year.asInt());
    }
};