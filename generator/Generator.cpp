#include "Tourney.cpp"
#include "Match.cpp"
#include<string>

using namespace std;

class Generator{
    struct PlaygroundInfo{
        Playground playground;
        PlayingDay actualPlayingDay;
        int playingDayIndex;
        int actualHour;
        int actualMinute;
        PlaygroundInfo(){
            playingDayIndex = 0;
        }
        PlaygroundInfo(Playground playground,
        int actualHour,
        int actualMinute){
            this->playground = playground;
            this->actualPlayingDay = playground.playingDays[0];
            this->actualHour = actualHour;
            this->actualMinute = actualMinute;
            playingDayIndex = 0;
        }
        void increasePlayingDay(){
            playingDayIndex++;
            if(playingDayIndex < playground.playingDays.size()){
                actualPlayingDay = playground.playingDays[playingDayIndex];
                actualHour = actualPlayingDay.hourFrom;
                actualMinute = actualPlayingDay.minuteFrom;
            }
        }
    };
    vector<PlaygroundInfo> playgroundInfo;
public:
    string convertMatchScheduleToJson(vector<Match> matches){
        unsigned int i;
        string s="{ \"matches\" : [";
        for(i = 0; i < matches.size(); i++){
            if(i == matches.size() - 1) {
                s+=matches[i].toJson()+"]}";
            } else {
                s+=matches[i].toJson()+",";
            }
        }
        return s;
    }
    vector<Match> generateMatchSchedule(Tourney tourney){
        unsigned int i;
        vector<Match> allMatchesAtTourney;
        for(i = 0; i < tourney.playgrounds.size(); i++){
            Playground playground = tourney.playgrounds[i];
            if(playground.playingDays.size() != 0){
                vector<PlayingDay> playingDays = playground.playingDays;
                PlaygroundInfo playgroundInfo(playground, playingDays[0].hourFrom,
                        playingDays[0].minuteFrom);
                this->playgroundInfo.push_back(playgroundInfo);
            }
        }
        for(i = 0; i < tourney.categories.size();i++){
            vector<Match> matches = matchesForCategory(tourney.playgrounds, tourney.categories[i], tourney.matchLength);
            unsigned int j;
            for(j = 0; j < matches.size(); j++){
                allMatchesAtTourney.push_back(matches[j]);
            }
        }
        return allMatchesAtTourney;
    }
    
    bool canAddMinutes(int hourFrom, int minuteFrom, int hourEnd, int minuteEnd, int length){
        int length2 = length;
        int hourTo = hourFrom;
        int minuteTo = minuteFrom;
        while(length > 60){
            hourTo++;
            length2-=60;
        }
        if(minuteFrom+length2 >= 60){
            hourTo++;
            minuteTo = minuteFrom+length2-60;
        } else {
            minuteTo = minuteFrom+length2;
        }
        if(hourTo < hourEnd || (hourTo == hourEnd && minuteTo <= minuteEnd)){
            return true;
        }
        return false;
    }
    
    Match matchWithReferee(vector<Playground> playgrounds, Category category, Group group, Team first, 
    Team second, int a, int b, int matchLength){
        unsigned int i, j;
        for(i = 0; i < category.teams.size();i++){
            if(i != a && i != b){
                for(j = 0; j < this->playgroundInfo.size(); j++){
                    int hour = this->playgroundInfo[j].actualHour;
                    int minute = this->playgroundInfo[j].actualMinute;
                    PlayingDay playingDay = this->playgroundInfo[j].actualPlayingDay;
                    if(!this->canAddMinutes(hour, minute, playingDay.hourTo, playingDay.minuteTo, matchLength)){
                        this->playgroundInfo[j].increasePlayingDay();
                    }
                }
                int playingDayIndex = this->playgroundInfo[0].playingDayIndex;
                int hourFrom = this->playgroundInfo[0].actualHour;
                int minuteFrom = this->playgroundInfo[0].actualMinute;
                Date earliestDate = this->playgroundInfo[0].actualPlayingDay.date;
                unsigned int index = 0;
                bool canSet = false;
                for(j = 0; j < this->playgroundInfo.size(); j++){
                    int hour = this->playgroundInfo[j].actualHour;
                    int minute = this->playgroundInfo[j].actualMinute;
                    int playingDayIndex2 = this->playgroundInfo[j].playingDayIndex;
                    Date date = this->playgroundInfo[j].actualPlayingDay.date;
                    if(playingDayIndex2 < this->playgroundInfo[j].playground.playingDays.size()){
                        canSet = true;
                    } else {
                        continue;
                    }
                    if((date.isEarlierOrSame(earliestDate) && !date.isSame(earliestDate)) || (date.isSame(earliestDate) && hour < hourFrom) || 
                            (date.isSame(earliestDate)  && hour == hourFrom && minute < minuteFrom)){
                        playingDayIndex = playingDayIndex2;
                        index = j;
                        hourFrom = hour;
                        minuteFrom = minute;
                        earliestDate = date;
                    }
                }
                if(!canSet){
                    throw "Nelze najít hrací den pro daný zápas.";
                }
                Match match(playgrounds[index], playgrounds[index].playingDays[playingDayIndex], first, second, category.teams[i], 
                        group.name, hourFrom, minuteFrom, matchLength);
                this->playgroundInfo[index].actualHour = match.hourTo;
                this->playgroundInfo[index].actualMinute = match.minuteTo;
                return match;
            }
        }
        throw "Nenalezen rozhodčí tým";
    }
    vector<Match> matchesForGroup(vector<Playground> playgrounds, Category category, Group group, int matchLength){
        unsigned int i,j, k, l, m;
        vector<Match> matches;
        for(i = 0; i < group.teams.size();i++){
            for(j = 0; j < i; j++){
                if(i != j){
                    for(m = 0; m < category.teams.size();m++){
                        if(category.teams[m].equals(group.teams[i])){
                            k = m;
                        }
                        if(category.teams[m].equals(group.teams[j])){
                            l = m;
                        }
                    }
                    Match match = matchWithReferee(playgrounds, category, group, 
                            group.teams[i], group.teams[j], k, l, matchLength);
                    matches.push_back(match);
                }
            }
        }
        return matches;
    }
    vector<Match> matchesForCategory(vector<Playground> playgrounds, Category category, int matchLength){
        unsigned int i,j;
        vector<Match> matches;
        for(i = 0; i < category.groups.size();i++){
            vector<Match> matchesOfGroup = this->matchesForGroup(playgrounds, category, category.groups[i],
                    matchLength);
            for(j = 0; j < matchesOfGroup.size(); j++){
                matches.push_back(matchesOfGroup[j]);
            }
        }
        return matches;
    }
}; 