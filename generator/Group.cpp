#ifndef TEAM
#define TEAM
#include "Team.cpp"
#endif
#include <string>

struct Group{
    string name;
    vector<Team> teams;
    Group(string name = ""){
        this->name = name;
    }
    Group(string name, vector<Team> teams){
        this->name = name;
        this->teams = teams;
    }
};