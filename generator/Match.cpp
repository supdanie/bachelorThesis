#ifndef PLAYGROUND
#define PLAYGROUND
#include "Playground.cpp"
#endif
#ifndef TEAM
#define TEAM
#include "Team.cpp"
#endif
#include<string>

struct Match{
    Playground playground;
    PlayingDay playingDay;
    Team firstTeam;
    Team secondTeam;
    Team referee;
    string group;
    int hourFrom;
    int minuteFrom;
    int hourTo;
    int minuteTo;
    void addMinutes(int hourFrom, int minuteFrom, int length){
        int length2 = length;
        int hourTo = this->hourFrom;
        int minuteTo = this->minuteFrom;
        while(length > 60){
            hourTo++;
            length2-=60;
        }
        if(minuteFrom+length2 >= 60){
            hourTo++;
            minuteTo = minuteFrom+length2-60;
        } else {
            minuteTo = minuteFrom+length2;
        }
        this->hourTo = hourTo;
        this->minuteTo = minuteTo;
    }
    Match(){
        
    }
    Match(Playground playground, PlayingDay playingDay, Team firstTeam, Team secondTeam, Team referee, 
            string group, int hourFrom, int minuteFrom, int length){
        this->playground = playground;
        this->playingDay = playingDay;
        this->firstTeam = firstTeam;
        this->secondTeam = secondTeam;
        this->referee = referee;
        this->group = group;
        this->hourFrom = hourFrom;
        this->minuteFrom = minuteFrom;
        addMinutes(hourFrom, minuteFrom, length);
    }
   
    string toJson(){
        string s = "{";
        s+=string("\"playground\": ")+to_string(this->playground.number)+",";
        s+=string("\"playingDay\": ")+to_string(this->playingDay.id)+",";
        s+=string("\"firstTeam\": ")+to_string(this->firstTeam.id)+",";
        s+=string("\"secondTeam\": ")+to_string(this->secondTeam.id)+",";
        s+=string("\"referee\": ")+to_string(this->referee.id)+",";
        s+=string("\"group\": \"")+this->group+"\",";
        s+=string("\"hourFrom\": ")+to_string(this->hourFrom)+",";
        s+=string("\"minuteFrom\": ")+to_string(this->minuteFrom)+"}";
        return s;
    }
};