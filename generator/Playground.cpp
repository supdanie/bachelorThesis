#include "PlayingDay.cpp"
#include <vector>

using namespace std;

struct Playground{
    vector<PlayingDay> playingDays;
    int number;
    Playground(){
        
    }
    Playground(vector<PlayingDay> playingDays, int number){
        this->playingDays = playingDays;
        this->number = number;
    }
    static Playground convertJson(Json::Value playingDays, Json::Value number){
        Json::Value::ArrayIndex i;
        vector<PlayingDay> playingDaysAtPlayground;    
        for(i = 0; i < playingDays.size(); i++){
            PlayingDay playingDay =  PlayingDay::convertJson(playingDays[i]["id"], playingDays[i]["date"], 
                    playingDays[i]["hourFrom"], playingDays[i]["minuteFrom"], playingDays[i]["hourTo"],
                    playingDays[i]["minuteTo"]);
            playingDaysAtPlayground.push_back(playingDay);
        }
        return Playground(playingDaysAtPlayground, number.asInt());
    }
};
