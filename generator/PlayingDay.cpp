#include "Date.cpp"
using namespace std;

struct PlayingDay{
    int id;
    Date date;
    int hourFrom;
    int minuteFrom;
    int hourTo;
    int minuteTo;
    PlayingDay(){
    }
    PlayingDay(int id, Date date, int hourFrom, int minuteFrom, int hourTo, int minuteTo){
        this->id = id;
        this->date = date;
        this->hourFrom = hourFrom;
        this->minuteFrom = minuteFrom;
        this->hourTo = hourTo;
        this->minuteTo = minuteTo;;
    }
    bool isPlayingDayBetween(Date first, Date second){
        if(this->date.year < first.year || this->date.year > second.year){
            return false;
        }
        if(this->date.year == first.year && this->date.month < first.month){
            return false;
        }
        if(this->date.year == second.year && this->date.month > second.month){
            return false;
        }
        if(this->date.year == first.year && this->date.month == first.month &&
                this->date.day < first.day){
            return false;
        }
        if(this->date.year == second.year && this->date.month == second.month && 
                this->date.day > second.day){
            return false;
        }
        return true;
    }
    bool isTimeInPlayingDay(int hour, int minute){
        if(hour < this->hourFrom && hour > this->hourTo){
            return false;
        }
        if(hour == this->hourFrom && minute < this->minuteFrom){
            return false;
        }
        if(hour == this->hourTo && minute > this->minuteTo){
            return false;
        }
        return true;
    }
    bool isEarlierThanStart(int hour, int minute){
        if(hour < this->hourFrom){
            return true;
        }
        if(hour == this->hourFrom && minute < this->minuteFrom){
            return true;
        }
        return false;
    }
    bool islaterThanEnd(int hour, int minute){
        if(hour > this->hourTo){
            return true;
        }
        if(hour == this->hourTo && minute > this->minuteTo){
            return true;
        }
        return false;
    }
    static PlayingDay convertJson(Json::Value id, Json::Value date, Json::Value hourFrom, Json::Value minuteFrom,
    Json::Value hourTo, Json::Value minuteTo){
        Date dateOfPlayingDay = Date::convertJson(date["day"],date["month"], date["year"]);
        return PlayingDay(id.asInt(),dateOfPlayingDay, hourFrom.asInt(), minuteFrom.asInt(), 
                hourTo.asInt(),minuteTo.asInt());
    }
};