#include<string>
#include <jsoncpp/json/json.h>

using namespace std;

struct Team{
    int id;
    string abbreviation;
    int numberOfPlayers;
    Team(int id = 0, string abbreviation = "", int numberOfPlayers = 0){
        this->id = id;
        this->abbreviation = abbreviation;
        this->numberOfPlayers = numberOfPlayers;
    }
    static Team convertJson(Json::Value id, Json::Value abbreviation, Json::Value numberOfPlayers){
        return Team(id.asInt(), abbreviation.asString(), numberOfPlayers.asInt());
    }
    
    bool equals(Team team){
        return team.id == this->id && team.abbreviation == this->abbreviation && 
                team.numberOfPlayers == this->numberOfPlayers;
    }
}; 