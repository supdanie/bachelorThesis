#ifndef PLAYGROUND
#define PLAYGROUND
#include "Playground.cpp"
#endif
#include "Category.cpp"
#include<iostream>
#include<vector>
#include <jsoncpp/json/json.h>

using namespace std;

struct Tourney{
    vector<Playground> playgrounds;
    vector<Category> categories;
    int matchLength;
    Date from;
    Date to;
    Tourney(vector<Playground> playgrounds, vector<Category> categories, int matchLength, Date from, Date to){
        this->playgrounds = playgrounds;
        this->categories = categories;
        this->matchLength = matchLength;
        this->from = from;
        this->to = to;
    }
    static Tourney convertJson(Json::Value categories, Json::Value playgrounds, 
    Json::Value matchLength, Json::Value from, Json::Value to){
        Json::Value::ArrayIndex i;
        vector<Category> categoriesAtTourney;
        vector<Playground> playgroundsAtTourney;
        for(i = 0; i < playgrounds.size(); i++){
            Playground playground = Playground::convertJson(playgrounds[i]["playingDays"],
                    playgrounds[i]["number"]);
            playgroundsAtTourney.push_back(playground);
        }
        Json::Value::ArrayIndex j;
        for(j = 0; j < categories.size(); j++){
            Category category = Category::convertJson(categories[j]["title"], 
                    categories[j]["numberOfGroups"],
                    categories[j]["teams"]);
            categoriesAtTourney.push_back(category);
        }
        Date fromDate = Date::convertJson(from["day"], from["month"], from["year"]);
        Date toDate = Date::convertJson(to["day"], to["month"], to["year"]);
        int matchLengthInteger = matchLength.asInt(); 
        return Tourney(playgroundsAtTourney, categoriesAtTourney, matchLengthInteger, fromDate, toDate);
    }
};