#include "Generator.cpp"
#include<iostream>
#include<string>

using namespace std;

string Category::actualGroup = "A";

int main(void){
    Json::Value val;
    cin >> val;
    Json::FastWriter fast;
    Json::StyledWriter styled;
    string sFast = fast.write(val);
    Json::Value categories = val["categories"];
    Json::Value playgrounds = val["playgrounds"];
    Json::Value matchLength = val["matchLength"];
    Json::Value from = val["from"];
    Json::Value to = val["to"];
    Tourney tourney = Tourney::convertJson(categories, playgrounds, matchLength, from, to);
    Generator generator;
    vector<Match> matches = generator.generateMatchSchedule(tourney);
    cout << generator.convertMatchScheduleToJson(matches) << endl;
    return 0;
}