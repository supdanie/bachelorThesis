$(function(){
    $( 'body' ).netteGMapPicker( {

        //my callback marker change position
        changePositionMarker: function( results ) {
            var district = results[4].formatted_address.split(",");
            //alert( district[0] );
            $("select#frm-addCompetitionForm-district_id option").each(function() { this.selected = ( this.text === district[0] ); });
            $("select#frm-editCompetitionForm-district_id option").each(function() { this.selected = ( this.text === district[0] ); });
            //alert('changePositionMarker');
        }

    } );
});